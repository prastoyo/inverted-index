import requests
from bs4 import BeautifulSoup
import mysql.connector
import re
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
import mysql.connector
import sys
from datetime import datetime
import uuid




def cleanhtml(raw_html):
  cleanr = re.compile('<.*?>')
  cleantext = re.sub(cleanr, '', raw_html)
  return cleantext
  
def cleanMe(soup):
    print("cleaning")
    #soup = BeautifulSoup(html) # create a new bs4 object from the html data loaded
    for script in soup(["script", "style"]): # remove all javascript and stylesheet code
      script.extract()
      # get text
      text = soup.get_text()
      # break into lines and remove leading and trailing space on each
      lines = (line.strip() for line in text.splitlines())
      # break multi-headlines into a line each
      chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
      # drop blank lines
      text = '\n'.join(chunk for chunk in chunks if chunk)
      return text

def invertedindex(soup):
    print("inverted")
    katadasar = cleanMe(soup).lower()
    print("sukses cleaned")
    #print(cleaned)
    # factory = StemmerFactory()
    # stemmer = factory.create_stemmer()
    # katadasar = stemmer.stem(cleaned)
    kata = re.split('\n| ',katadasar)
    print("sukses stemming")
    for team in [ele for ind, ele in enumerate(kata,1) if ele not in kata[ind:]]:
        #print("{} {}".format(team,kata.count(team)))
        mySql_insert_query = """INSERT INTO inverted_index (id, term ,url, frekuensi) VALUES (%s, %s, %s, %s) """
        recordTuple = ('', str(team), str(url) ,int(kata.count(team)))
        cursor = connection.cursor()
        cursor.execute(mySql_insert_query, recordTuple)
        connection.commit()
    print("sukses masukdata inverted")

def execdatabase(mySql_insert_query,recordTuple):
    print("insert data")
    cursor = connection.cursor()
    cursor.execute(mySql_insert_query, recordTuple)
    connection.commit()
    print("sukses masuk data content")

def crawler(url,depth,headers):
    now = datetime.now()
    for i in range(depth):
        print(i);
        if i == 0:
            listlink.append([])
            page = requests.get(url, headers)
            soup = BeautifulSoup(page.content, 'html.parser')
            # inverted = invertedindex(soup)
            title = soup.find('title')
            stringtitle = title.string
            print(stringtitle)
            tb = soup.find_all('table', class_='')
            if tb == "":
              pass     
            for tables in tb:
                # sys.exit("Error message")word
                id_table = 'table_'+str(uuid.uuid4())
                id_table = id_table.replace("-","")
                tbs = BeautifulSoup(str(tb[0]),'lxml')
                tr = tbs.find_all('tr')
                #Mendapat Kolom Tabel
                x = 0
                for trz in tr:
                    if( x == 0):
                        trs = BeautifulSoup(str(trz),'lxml')
                        th = tbs.find_all('th')
                        z = 1
                        column = ''
                        for ths in th:
                            if(z == len(th)):
                                word = re.sub(r'[^\w]', ' ', cleanhtml(str(ths)))
                                word = word.replace(" ","_")
                                column = column+word+" text "
                            else:
                                word = re.sub(r'[^\w]', ' ', cleanhtml(str(ths)))
                                word = word.replace(" ","_")
                                column = column+re.sub(r'[^\w]', ' ', word)+" text "+', '
                            z = z + 1
                        mySql_insert_query = "CREATE TABLE "+id_table+" ( "+column+" ) "
                        cursor = connection.cursor()
                        cursor.execute(mySql_insert_query)
                        connection.commit()
                        mySql_insert_query = """INSERT INTO page ( url ,title, description, id_table) VALUES ( %s, %s, %s, %s) """
                        recordTuple = ( url, str(stringtitle) ,str(tables),str(id_table))
                        execdata = execdatabase(mySql_insert_query,recordTuple)
                    #Mendapat Row Tabel
                    else:
                        trs = BeautifulSoup(str(trz),'lxml')
                        th = trs.find_all('td')
                        z = 1
                        column = ''
                        for ths in th:
                            if(z == len(th)):
                                word = re.sub(r'[^\w]', ' ', cleanhtml(str(ths)))
                                column = column+"\""+word+"\""
                            else:
                                word = re.sub(r'[^\w]', ' ', cleanhtml(str(ths)))
                                column = column+"\""+re.sub(r'[^\w]', ' ', word)+"\""+', '
                            z = z + 1
                        query = "INSERT INTO "+id_table+" VALUES ("+column+" ) "
                        print(query)
                        cursor.execute(query)
                        connection.commit()
                    x = x + 1
                tables.attrs = {}
                #untuk input database   
            link = soup.find_all('a')
            for links in link:
                isilink = links.get('href')
                if isilink:
                    if 'uns.ac.id' in isilink:
                        listlink[i].append(isilink)
        if i > 0:
                c = i - 1
                listlink.append([])
                for url in listlink[c]:
                    try:
                        print(url)
                        page = requests.get(url)
                        soup = BeautifulSoup(page.content, 'html.parser')
                        # inverted = invertedindex(soup)
                        title = soup.find('title')
                        stringtitle = title.string
                        tb = soup.find_all('table', class_='')
                        for tables in tb:
                            print('ASUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU')
                            # sys.exit("Error message")
                            id_table = 'table_'+str(uuid.uuid4())
                            id_table = id_table.replace("-","")
                            tbs = BeautifulSoup(str(tb[0]),'lxml')
                            tr = tbs.find_all('tr')
                            #Mendapat Kolom Tabel
                            x = 0
                            for trz in tr:
                                if( x == 0):
                                    trs = BeautifulSoup(str(trz),'lxml')
                                    th = tbs.find_all('th')
                                    if(len(th) == 0){
                                        th = tbs.find_all('tr')
                                    }
                                    z = 1
                                    column = ''
                                    for ths in th:
                                        if(z == len(th)):
                                            word = re.sub(r'[^\w]', ' ', cleanhtml(str(ths)))
                                            word = word.replace(" ","_")
                                            column = column+word+" text "
                                        else:
                                            word = re.sub(r'[^\w]', ' ', cleanhtml(str(ths)))
                                            word = word.replace(" ","_")
                                            column = column+re.sub(r'[^\w]', ' ', word)+" text "+', '
                                        z = z + 1
                                    mySql_insert_query = "CREATE TABLE "+id_table+" ( "+column+" ) "
                                    print(mySql_insert_query)
                                    cursor = connection.cursor()
                                    cursor.execute(mySql_insert_query)
                                    connection.commit()
                                    mySql_insert_query = """INSERT INTO page (id, url ,title, description, id_table) VALUES (%s, %s, %s, %s, %s) """
                                    recordTuple = ('', url, str(stringtitle) ,str(tables),str(id_table))
                                    execdata = execdatabase(mySql_insert_query,recordTuple)
                                #Mendapat Row Tabel
                                else:
                                    trs = BeautifulSoup(str(trz),'lxml')
                                    th = trs.find_all('td')
                                    z = 1
                                    column = ''
                                    for ths in th:
                                        if(z == len(th)):
                                            word = re.sub(r'[^\w]', ' ', cleanhtml(str(ths)))
                                            column = column+"\""+word+"\""
                                        else:
                                            word = re.sub(r'[^\w]', ' ', cleanhtml(str(ths)))
                                            column = column+"\""+re.sub(r'[^\w]', ' ', word)+"\""+', '
                                        z = z + 1
                                    query = "INSERT INTO "+id_table+" VALUES ("+column+" ) "
                                    cursor.execute(query)
                                    connection.commit()
                                x = x + 1
                            tables.attrs = {}
                            #untuk input database   
                        link = soup.find_all('a')
                        for links in link:
                            isilink = links.get('href')
                            if isilink:
                                if 'uns.ac.id' in isilink:
                                    listlink[i].append(isilink)
                    except Exception as ex:
                        print(ex)


connection = mysql.connector.connect(host='localhost', database='inverted', user='user', password='moechammad')
headers={"User-Agent": "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36"}
url = "https://uns.ac.id/id/akademik/akreditasi-program-studi-di-program-sarjana-uns.html"
#untuk kedalaman
depth = 1
listlink=[]
craw = crawler(url,depth,headers)
# listlink.append([])
# listlink[0].append('sad')
# print(listlink[0][0])




