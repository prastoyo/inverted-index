import requests
from bs4 import BeautifulSoup
import mysql.connector
import re
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
import mysql.connector

def cleanMe(soup):
	print("cleaning")

	for script in soup(["script", "style"]):
		script.extract()
		text = soup.get_text()
		lines = (line.strip() for line in text.splitlines())
		chunks = (phrase.strip() for line in lines for phrase in line.split(" "))
		text = '\n'.join(chunk for chunk in chunks if chunk)
		return text

def invertedindex(soup):
	print("inverted")
	katadasar = cleanMe(soup).lower()
	print("sukses cleaned")
	kata = re.split('\n| ',katadasar)
	print("sukses stemming")
	for team in [ele for ind, ele in enumerate(kata,1) if ele not in kata[ind:]]:
		mySql_insert_query = """INSERT INTO inverted_index (id, term, url, frekuensi) VALUES (%s, %s, %s, %s) """
		recordTuple = ('', str(team), str(url), int(kata.count(team)))
		cursor = connection.cursor()
		cursor.execute(mySql_insert_query, recordTuple)
		connection.commit()
	print("sukses masukdata inverted")

def execdatabase(mySql_insert_query,recordTuple):
	print("insert data")
	cursor = connection.cursor()
	cursor.execute(mySql_insert_query, recordTuple)
	connection.commit()
	print("sukses masuk data content")

def crawler(url,depth,headers):
	for i in range(depth):
		print(i);
		if i == 0:
			listlink.append([])
			page = requests.get(url, headers)
			soup = BeautifulSoup(page.content, 'html.parser')
			
			title = soup.find('title')
			stringtitle = title.string
			print(stringtitle)
			tb = soup.find_all('table', class_='')

			if tb == "":
			  pass
			for tables in tb:
				inverted = invertedindex(tables)
				tables.attrs = {}
				mySql_insert_query = """INSERT INTO content (id, url, title, content) VALUES (%s, %s, %s, %s) """
				recordTuple = ('', url, str(stringtitle), str(tables))
				execdata = execdatabase(mySql_insert_query,recordTuple)
			link = soup.find_all('a')
			for links in link:
				isilink = links.get('href')
				if isilink:
					if 'uns.ac.id' in isilink:
						listlink[i].append(isilink)
		if i > 0:
				c = i - 1
				listlink.append([])
				for url in listlink[c]:
					try:
						print(url)
						page = requests.get(url)
						soup = BeautifulSoup(page.content, 'html.parser')
						
						title = soup.find('title')
						stringtitle = title.string
						tb = soup.find_all('table', class_ ='')
						for tables in tb:
							inverted = invertedindex(tables)
							tables.attrs = {}
							mySql_insert_query = """INSERT INTO content (id, url, title, content) VALUES (%s, %s, %s, %s) """
							recordTuple = ('', url, str(stringtitle),  str(tables))
							execdata = execdatabase(mySql_insert_query,recordTuple)
						link = soup.find_all('a')
						for links in link:
							isilink = links.get('href')
							if isilink:
								if 'uns.ac.id' in isilink:
									listlink[i].append(isilink)
					except(ConnectionError, Exception):
						print("Exception is :")

connection = mysql.connector.connect(host='localhost', database='dbcrawler', user='root', password='')
headers = {"User-Agent": "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36"}
url = "https://mipa.uns.ac.id"

depth = 2
listlink=[]
craw = crawler(url,depth,headers)