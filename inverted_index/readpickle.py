import pickle
import json


objects = []
with (open("code_from_url_dict.pickle", "rb")) as openfile:
    while True:
        try:
            objects.append(pickle.load(openfile))
        except EOFError:
            break

with open('code_from_url_dict.json', 'w') as outfile:
    json.dump(objects, outfile)

