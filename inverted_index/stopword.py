from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory
import pickle 
factory = StopWordRemoverFactory()
stopwords = factory.get_stop_words()
print(stopwords)

objects = []
with (open("inverted_index_dict.pickle", "rb")) as openfile:
    while True:
        try:
            objects.append(pickle.load(openfile))
        except EOFError:
            break

print(objects)