<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>Fakultas Ilmu Budaya, Universitas Sebelas Maret</title>

<!-- Google Fonts -->
<link href='https://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

<!-- Plugin CSS -->
<link href="https://fib.uns.ac.id/vendor/bxslider/jquery.bxslider.min.css" rel="stylesheet" />
<link href="https://fib.uns.ac.id/vendor/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet" />
<link href="https://fib.uns.ac.id/vendor/fancybox/jquery.fancybox.min.css" rel="stylesheet" />

<!-- Theme CSS - Includes Bootstrap -->
<link href="https://fib.uns.ac.id/css/style.min.css" rel="stylesheet">
<link href="https://fib.uns.ac.id/css/custom.css" rel="stylesheet"></head>

<body id="page-top">
  <header>
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg py-3" id="mainNav">
    <div class="container d-flex flex-column flex-md-row">
      <div class="row w-100 h-80 m-0">
        <div class="col-md-3 pl-0">
          <a class="py-2" href="https://fib.uns.ac.id" aria-label="FIB UNS">
            <img src="https://fib.uns.ac.id/img/logo-header.png" img-default="https://fib.uns.ac.id/img/logo-header.png" img-white="https://fib.uns.ac.id/img/logo-header-wh.png" class="navbrand">
          </a>
        </div>
        <div class="col-md-9 pr-0 text-right">
          <ul class="nav-menu mr-3">
            <li><a class="d-md-inline-block" href="https://fib.uns.ac.id/galleries">Galeri</a></li>
            <li><a class="d-md-inline-block" href="https://fib.uns.ac.id/news/category/unduhan">Unduhan</a></li>
            <li><a class="d-md-inline-block" href="https://fib.uns.ac.id/contact">Kritik & Saran</a></li>
          </ul>

          <div class="float-menu">
            <button type="button" class="btn btn-primary mr-1" id="float-search" data-toggle="tooltip" data-placement="bottom" title="Pencarian"><span class="icon search"></span></button>
            <button type="button" class="btn btn-primary" id="float-main" data-toggle="tooltip" data-placement="bottom" title="Menu utama"><span class="icon mainmenus"></span></button>
          </div>
        </div>
      </div>
    </div>
  </nav>
</header>

<!-- Search -->
<section class="page-section" id="search">
  <div class="container">
    <h4>Kata kunci pencarian</h4>
    <form class="form-inline" action="https://fib.uns.ac.id/news">
      <div class="form-group">
        <input type="text" name="search" value="" class="form-control" id="keyword" placeholder="keyword">
      </div>
      <button type="submit" class="btn btn-primary btn-xl pull-right">Cari</button>
    </form>
  </div>

  <div class="float-menu">
    <div class="container">
      <button type="button" class="btn btn-warning mr-1" id="float-search-close" data-toggle="tooltip"
        data-placement="bottom" title="Tutup pencarian"><span class="icon search-close"></span></button>
      <button type="button" class="btn btn-primary" id="float-main2" data-toggle="tooltip" data-placement="bottom"
        title="Tutup menu utama"><span class="icon mainmenus"></span></button>
    </div>
  </div>
</section>

<!-- Main menu -->
<section class="page-section" id="mainMenu">
  <div class="container is-relative h-100">
    <div class="menu-list">
      <a class="py-2" href="#" aria-label="FIB UNS">
        <img src="https://fib.uns.ac.id/img/logo-header-wh.png" class="navbrand">
      </a>
      <ul>
        <li><a href="https://fib.uns.ac.id">Beranda</a></li>
        <li class="mb-2">
          <a href="https://fib.uns.ac.id/page/profile-fakultas">Profil FIB</a>
          <ul class="mt-2 ml-3">
            <li class="mb-2"><a href="https://fib.uns.ac.id/page/visi-misi-tujuan"><small>Visi, Misi &amp; Tujuan</small></a></li>
            <li class="mb-2"><a href="https://fib.uns.ac.id/page/program-unggulan"><small>Program Unggulan</small></a></li>
          </ul>
        </li>
        <li><a href="https://fib.uns.ac.id/news/category/program-studi">Program Studi</a></li>
        <li><a href="https://fib.uns.ac.id/news/category/akademik">Akademik</a></li>
        <li><a href="https://fib.uns.ac.id/news/category/kemahasiswaan">Kemahasiswaan</a></li>
        <li><a href="https://fib.uns.ac.id/galleries">Galeri</a></li>
        <li><a href="https://fib.uns.ac.id/news/category/unduhan">Unduhan</a></li>
        <li><a href="https://fib.uns.ac.id/news/category/materi-kuliah">Materi Kuliah</a></li>
        <li><a href="https://fib.uns.ac.id/contact">Kritik &amp; Saran</a></li>
      </ul>
    </div>

    <div class="side-info">
      <div class="contact">
        <h5>Fakultas Ilmu Budaya</h5>
        <h3>Universitas Sebelas Maret</h3>
        <p>
          Jl. Ir. Sutami No. 36A Kentingan, Jebres,<br>
          Surakarta<br>
          Telp. 0271 635236<br>
          Email : fib@mail.uns.ac.id
        </p>
      </div>
      
      <div class="stats">
        <div class="stat">
          <span class="icon program"></span>
          <span class="count">7</span>
          <span class="title">Program Studi</span>
        </div>

        <div class="stat">
          <span class="icon student"></span>
          <span class="count"><u>+</u> 1500</span>
          <span class="title">Mahasiswa Aktif</span>
        </div>

        <div class="stat">
          <span class="icon doctor"></span>
          <span class="count">32</span>
          <span class="title">Doktor</span>
        </div>

        <div class="stat">
          <span class="icon professor"></span>
          <span class="count">12</span>
          <span class="title">Guru Besar</span>
        </div>
      </div>
    </div>
  </div>

  <div class="float-menu">
    <div class="container">
      <button type="button" class="btn btn-primary mr-1" id="float-search2" data-toggle="tooltip" data-placement="bottom" title="Pencarian">
        <span class="icon search"></span>
      </button>
      <button type="button" class="btn btn-warning" id="float-main-close" data-toggle="tooltip" data-placement="bottom" title="Tutup menu utama">
        <span class="icon mainmenus-close"></span>
      </button>
    </div>
  </div>
</section>  
  <!-- Page heading Section -->
<section class="page-section" id="page-heading">
    <div class="container is-relative h-100">
        <h2>News Detail</h2>
    </div>
</section>

<!-- Page Section -->
<section class="page-section pt-0" id="pages">
    <div class="detail-page">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="article-content">
                        <img src="https://fib.uns.ac.id/storage/posts/September2020/dWS2fAEC6NMNYYo4BInV.png" alt="Gempita Semangat PKKMB FIB UNS" draggable="false">

                        <h2 class="article-title mt-4 mb-4" style="min-height: 75px;">
                            Gempita Semangat PKKMB FIB UNS
                            <div class="article-meta">
                                <span>23</span>
                                <span>SEP</span>
                            </div>
                        </h2>

                        <div class="article-category">
                            <a href="https://fib.uns.ac.id/news/category/kegiatan">Kegiatan</a>
                        </div>

                        <p class="MsoNormal" style="text-align: justify; line-height: 150%;"><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 150%; font-family: 'Times New Roman','serif';">Mentari kembali merangkak di sudut horizon, gempita semangat terpancar lewat perwakilan senyum dari para mahasiswa baru dalam bingkai media Zoom Cloud Meeting. Akibat pandemi Covid 19 nyaris semua akitifitas dialihkan lewat dimensi <em style="mso-bidi-font-style: normal;">online</em>, begitu juga Pengenalan Kehidupan Kampus Mahasiswa Baru (PKKMB) 2020 di Fakultas Ilmu Budaya (FIB), Universitas Sebelas Maret (UNS) yang dilakukan pada, Rabu-Kamis (16-17/09/2020).</span></p>
<p class="MsoNormal" style="line-height: 150%; text-align: center;"><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 150%; font-family: 'Times New Roman','serif';"><img src="https://fib.uns.ac.id/storage/posts/September2020/Untitled.png" alt="" width="398" height="183" /></span></p>
<p class="MsoNormal" style="text-align: justify; line-height: 150%;"><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 150%; font-family: 'Times New Roman','serif';">Langit biru dengan gradasi awan putih memayungi gedung III FIB, menjadi <em style="mso-bidi-font-style: normal;">backgroud virtual</em> yang diseragamkan dalam PKKMB<span style="mso-spacerun: yes;">&nbsp; </span>2020 secara daring, lantunan selamat dan semangat dengan apik juga disampaikan oleh Prof. Dr. Warto, M.Hum, Dekan FIB. Bagi mahasiswa baru, FIB adalah pilihan tepat, ditengah gejolak perubahan masyarakat yang kurang memperhatikan aspek kebudayaan.<span style="mso-spacerun: yes;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></p>
<p class="MsoNormal" style="text-align: justify; line-height: 150%;"><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 150%; font-family: 'Times New Roman','serif';">Dalam pemaparannya Prof. Warto juga menyinggung tentang </span><span style="font-size: 12.0pt; line-height: 150%; font-family: 'Times New Roman','serif';">program dari <span style="background: white;">Kementerian Pendidikan dan Kebudayaan perihal<em style="mso-bidi-font-style: normal;"> Kampus Merdeka</em>, </span></span><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 150%; font-family: 'Times New Roman','serif';">guna memajukan peradaban mahasiswa generasi saat ini, harus mampu membuka khazanah dengan pelbagai pengetahuan yang lebih variable. <span style="mso-spacerun: yes;">&nbsp;</span>&ldquo;Maksud dari <em style="mso-bidi-font-style: normal;">Kampus Merdeka</em> itu anda tidak hanya diajari berenang di kolam yang sempit, namun anda dipersiapkan untuk berenang melintas selat. <em style="mso-bidi-font-style: normal;">Nah</em>, ini melatih sisi ketahanan, gaya berenang harus bermacam-macam, cara mengatasi tekanan dan tantangan apapun ketika kalian berenang di laut nantinya, kalau tidak dipersiapkan secara baik kalian akan tenggelam&rdquo; jelasnya.</span></p>
<p class="MsoNormal" style="line-height: 150%;"><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 150%; font-family: 'Times New Roman','serif';"><img style="display: block; margin-left: auto; margin-right: auto;" src="https://fib.uns.ac.id/storage/posts/September2020/pkkmb 2.png" alt="" width="379" height="213" /></span></p>
<p class="MsoNormal" style="text-align: justify; line-height: 150%;"><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 150%; font-family: 'Times New Roman','serif';">Motivasi dengan nada yang seirama disampaikan oleh Prof. Dr. Tri Wiratno, M.A, Wakil Dekan Bidang Akademik FIB, melalui pantauannya mahasiswa FIB terbukti mampu menyelesaikan permasalahan sosial. &ldquo;Kenapa memilih Fakultas Ilmu Budaya tidak fakultas yang lain ? Fakultas kita itu belajar tentang ilmu humaniora, budaya, filsafat dan sosial. Itu akan menjadi pengetahuan akumulatif, kita mempunyai wawasan luas tentang apa yang dijalanai sehari-hari. Lewat pengalaman saya membimbing KKM, mahasiswa FIB lebih mampu <span style="mso-spacerun: yes;">&nbsp;</span>menyelesaikan permasalahan sosial yang ada di sana&rdquo; ungkapnya. </span></p>
<p class="MsoNormal" style="line-height: 150%;"><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 150%; font-family: 'Times New Roman','serif';">&nbsp;<img style="display: block; margin-left: auto; margin-right: auto;" src="https://fib.uns.ac.id/storage/posts/September2020/pkkmb 3.png" alt="" width="360" height="201" /></span></p>
<p class="MsoNormal" style="text-align: justify; line-height: 150%;"><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 150%; font-family: 'Times New Roman','serif';">Acara PKKMB 2020 berlanjut pada pemamaran Informasi Bidang Akademik yang disampaikan oleh, Kasub Akademik, Maretta Elly Susilowati, S.E, kemudian penjelasan mengenai Sistem Informasi dan Administrasi Akademik (Siakad) dijelaskan oleh Laksono Widiyanto. Sinar<span style="mso-spacerun: yes;">&nbsp; </span>matahari kian berjalan, hal itu agaknya tidak menambah efek lelah, malah teriknya membakar semangat mahasiswa baru, yang dengan sekasama mendengarkan pemamaran tentang Menejemen Keuangan dan Sistem Informasi FIB UNS, oleh Wakil Dekan Bidang Umum dan Keuangan, Prof. Dr. Wakit, M.Hum.</span></p>
<p class="MsoNormal" style="line-height: 150%;"><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 150%; font-family: 'Times New Roman','serif';"><img style="display: block; margin-left: auto; margin-right: auto;" src="https://fib.uns.ac.id/storage/posts/September2020/pkkmb 4.png" alt="" width="429" height="241" /></span></p>
<p class="MsoNormal" style="text-align: justify; line-height: 150%;"><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 150%; font-family: 'Times New Roman','serif';">Sebelum rehat ISHOMA Wakil Dekan Bidang Kemahasiswaan dan Alumni, Dr. Henry Yustanto, M.A menjabarkan materi tentang Potensi, Minat, dan Bakat Mahasiswa. Senyum ramah dan penjelasan yang renyah, layaknya<span style="mso-spacerun: yes;">&nbsp; </span>merangkul dengan<span style="mso-spacerun: yes;">&nbsp; </span>nyaman seluruh mahasiswa baru FIB tahun akademik 2020/2021. Menurutnya, minat yang didorong dengan potensi akan menghasilkan sesuatu dengan maksimal. &ldquo;Minat yang sudah anda putuskan itu didorong dengan potensi yang ada dalam pribadi masing-masing itu akan menghasilkan suatu kinerja yang sangat maksimal. Mulai dari potensi, minat dan bakat jika semuanya bersinergi pasti akan melahirkan mahasiswa yang baik&rdquo; terangnya.</span></p>
<figure class="image align-center"><img style="width: 589px; height: 332px; display: block; margin-left: auto; margin-right: auto;" src="https://fib.uns.ac.id/storage/posts/September2020/pkkmb 5.png" alt="" width="552" height="311" />
<figcaption><span style="text-align: justify; font-family: 'Times New Roman', serif; font-size: 12pt;">Dinar Winahyu Damayanti, MC PKKMB FIB. Menyemangati Maba&nbsp;</span><span style="font-family: 'Times New Roman', serif; font-size: 12pt; text-align: left;">dengan Pekik &ldquo;Mahasiswa Baru Pasti, FIB Bisa&rdquo;</span></figcaption>
</figure>
<p class="MsoNormal" style="text-align: justify; line-height: 150%;"><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 150%; font-family: 'Times New Roman','serif';">Pekik &ldquo;Mahasiswa Baru Pasti, FIB Bisa&rdquo; agaknya menjadi awal dentum semangat. Kedepan mahasiswa FIB diharap pasti bisa menyelesaikan tantangan kompleks yang mereka hadapi. Mulai dari, sisi akademik maupun aspek lain, hingga kelak melahirkan lulusan yang kompeten dan memajukan peradaban.</span></p>
<p class="MsoNormal" style="text-align: justify; line-height: 150%;"><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 150%; font-family: 'Times New Roman','serif';">Dapat diinformasikan Rekap Data Mahasiswa Baru Tahun 2020 Fakultas Ilmu Budaya, sebagai berikut :</span></p>
<table class="MsoTableGrid" style="width: 470.4pt; border-collapse: collapse; border: none; mso-border-alt: solid windowtext .5pt; mso-yfti-tbllook: 1184; mso-padding-alt: 0in 5.4pt 0in 5.4pt;" border="1" width="627" cellspacing="0" cellpadding="0">
<tbody>
<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes;">
<td style="width: 34.15pt; border: solid windowtext 1.0pt; mso-border-alt: solid windowtext .5pt; padding: 0in 5.4pt 0in 5.4pt;" valign="top" width="46">
<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify; line-height: 150%;"><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 150%; font-family: 'Times New Roman','serif';">NO</span></p>
</td>
<td style="width: 193.5pt; border: solid windowtext 1.0pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0in 5.4pt 0in 5.4pt;" valign="top" width="258">
<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify; line-height: 150%;"><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 150%; font-family: 'Times New Roman','serif';">PRODI</span></p>
</td>
<td style="width: 242.75pt; border: solid windowtext 1.0pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0in 5.4pt 0in 5.4pt;" valign="top" width="324">
<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify; line-height: 150%;"><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 150%; font-family: 'Times New Roman','serif';">JUMLAH</span></p>
</td>
</tr>
<tr style="mso-yfti-irow: 1; height: 102.55pt;">
<td style="width: 34.15pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: solid black 1.0pt; mso-border-bottom-themecolor: text1; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; mso-border-bottom-alt: solid black .5pt; padding: 0in 5.4pt 0in 5.4pt; height: 102.55pt;" valign="top" width="46">
<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify; line-height: 150%;"><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 150%; font-family: 'Times New Roman','serif';">1.</span></p>
<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify; line-height: 150%;"><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 150%; font-family: 'Times New Roman','serif';">2.</span></p>
<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify; line-height: 150%;"><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 150%; font-family: 'Times New Roman','serif';">3.</span></p>
<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify; line-height: 150%;"><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 150%; font-family: 'Times New Roman','serif';">4.</span></p>
<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify; line-height: 150%;"><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 150%; font-family: 'Times New Roman','serif';">5.</span></p>
</td>
<td style="width: 193.5pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; mso-border-bottom-themecolor: text1; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; mso-border-bottom-alt: solid black .5pt; padding: 0in 5.4pt 0in 5.4pt; height: 102.55pt;" valign="top" width="258">
<table class="MsoNormalTable" style="width: 134.1pt; border-collapse: collapse; height: 90px;" border="0" width="179" cellspacing="0" cellpadding="0">
<tbody>
<tr style="height: 18px;">
<td style="width: 163.403px; padding: 0in 5.4pt; height: 18px;" valign="bottom" nowrap="nowrap">
<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 14.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman'; color: black;">Sastra Daerah</span></p>
</td>
</tr>
<tr style="height: 18px;">
<td style="width: 163.403px; padding: 0in 5.4pt; height: 18px;" valign="bottom" nowrap="nowrap">
<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 14.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman'; color: black;">Sastra Indonesia</span></p>
</td>
</tr>
<tr style="height: 18px;">
<td style="width: 163.403px; padding: 0in 5.4pt; height: 18px;" valign="bottom" nowrap="nowrap">
<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 14.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman'; color: black;">Sastra Inggris</span></p>
</td>
</tr>
<tr style="height: 18px;">
<td style="width: 163.403px; padding: 0in 5.4pt; height: 18px;" valign="bottom" nowrap="nowrap">
<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 14.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman'; color: black;">Ilmu Sejarah</span></p>
</td>
</tr>
<tr style="height: 18px;">
<td style="width: 163.403px; padding: 0in 5.4pt; height: 18px;" valign="bottom" nowrap="nowrap">
<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 14.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman'; color: black;">Sastra Arab</span></p>
</td>
</tr>
</tbody>
</table>
<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify; line-height: 150%;">&nbsp;</p>
</td>
<td style="width: 242.75pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; mso-border-bottom-themecolor: text1; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; mso-border-bottom-alt: solid black .5pt; padding: 0in 5.4pt 0in 5.4pt; height: 102.55pt;" valign="top" width="324">
<table class="MsoNormalTable" style="width: 48.0pt; border-collapse: collapse; mso-yfti-tbllook: 1184; mso-padding-alt: 0in 5.4pt 0in 5.4pt;" border="0" width="64" cellspacing="0" cellpadding="0">
<tbody>
<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes; height: 18.75pt;">
<td style="width: 48.0pt; background: white; mso-background-themecolor: background1; padding: 0in 5.4pt 0in 5.4pt; height: 18.75pt;" valign="bottom" nowrap="nowrap" width="64">
<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: right; line-height: normal;" align="right"><span style="font-size: 14.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman'; color: black; mso-themecolor: text1;">73</span></p>
</td>
</tr>
<tr style="mso-yfti-irow: 1; height: 18.75pt;">
<td style="width: 48.0pt; background: white; mso-background-themecolor: background1; padding: 0in 5.4pt 0in 5.4pt; height: 18.75pt;" valign="bottom" nowrap="nowrap" width="64">
<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: right; line-height: normal;" align="right"><span style="font-size: 14.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman'; color: black; mso-themecolor: text1;"><span style="mso-spacerun: yes;">&nbsp; </span>75</span></p>
</td>
</tr>
<tr style="mso-yfti-irow: 2; height: 18.75pt;">
<td style="width: 48.0pt; background: white; mso-background-themecolor: background1; padding: 0in 5.4pt 0in 5.4pt; height: 18.75pt;" valign="bottom" nowrap="nowrap" width="64">
<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: right; line-height: normal;" align="right"><span style="font-size: 14.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman'; color: black; mso-themecolor: text1;">64</span></p>
</td>
</tr>
<tr style="mso-yfti-irow: 3; height: 18.75pt;">
<td style="width: 48.0pt; background: white; mso-background-themecolor: background1; padding: 0in 5.4pt 0in 5.4pt; height: 18.75pt;" valign="bottom" nowrap="nowrap" width="64">
<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: right; line-height: normal;" align="right"><span style="font-size: 14.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman'; color: black; mso-themecolor: text1;">58</span></p>
</td>
</tr>
<tr style="mso-yfti-irow: 4; mso-yfti-lastrow: yes; height: 18.75pt;">
<td style="width: 48.0pt; background: white; mso-background-themecolor: background1; padding: 0in 5.4pt 0in 5.4pt; height: 18.75pt;" valign="bottom" nowrap="nowrap" width="64">
<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: right; line-height: normal;" align="right"><span style="font-size: 14.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman'; color: black; mso-themecolor: text1;">61</span></p>
</td>
</tr>
</tbody>
</table>
<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify; line-height: 150%;">&nbsp;</p>
</td>
</tr>
<tr style="mso-yfti-irow: 2; mso-yfti-lastrow: yes; height: 33.3pt;">
<td style="width: 34.15pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid black .5pt; mso-border-top-themecolor: text1; mso-border-alt: solid windowtext .5pt; padding: 0in 5.4pt 0in 5.4pt; height: 33.3pt;" valign="top" width="46">
<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify; line-height: 150%;"><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 150%; font-family: 'Times New Roman','serif';">&nbsp;</span></p>
</td>
<td style="width: 193.5pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-top-themecolor: text1; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0in 5.4pt 0in 5.4pt; height: 33.3pt;" valign="top" width="258">
<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: normal;" align="center"><span style="font-size: 14.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman'; color: black;">Total</span></p>
</td>
<td style="width: 242.75pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-top-themecolor: text1; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0in 5.4pt 0in 5.4pt; height: 33.3pt;" valign="top" width="324">
<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 14.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman'; color: black; mso-themecolor: text1;"><span style="mso-spacerun: yes;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>331</span></p>
</td>
</tr>
</tbody>
</table>
<p class="MsoNormal" style="text-align: justify; line-height: 150%;"><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 150%; font-family: 'Times New Roman','serif';"><span style="mso-spacerun: yes;">&nbsp; </span></span></p>
                    </div>
                </div>

                <div class="col-md-4">
                    <h4 class="mb-4 mt-4">Berita Lainnya</h4>
                    <div class="article-list">
                                                    <div class="article-item">
                                <div class="article-media">
                                    <img src="https://fib.uns.ac.id/storage/posts/July2020/Giqu4zo1ybN5lBM3WIoC.jpeg" alt="news-fib">
                                </div>

                                <div class="article-title">
                                    <div class="article-meta">
                                        <span>04</span>
                                        <span>DES</span>
                                    </div>
                                    <h2><a href="https://fib.uns.ac.id/news/mahasiswa-sastra-indonesia-fib-uns-terbitkan-artikel-di-jurnal-terakreditasi-nasional">Mahasiswa Sastra Indonesia FIB UNS Terbitkan Artikel di Jurnal Terakreditasi Nasional</a></h2>
                                    <div class="category">
                                        <a href="https://fib.uns.ac.id/news/category/kegiatan">Kegiatan</a>
                                    </div>
                                </div>
                            </div>
                                                    <div class="article-item">
                                <div class="article-media">
                                    <img src="https://fib.uns.ac.id/storage/posts/July2020/JZQCVwpSyrDODydJ4CVA.jpg" alt="news-fib">
                                </div>

                                <div class="article-title">
                                    <div class="article-meta">
                                        <span>15</span>
                                        <span>APR</span>
                                    </div>
                                    <h2><a href="https://fib.uns.ac.id/news/mahasiswa-sastra-indonesia-uns-raih-juara-2-lomba-menulis-cerpen">Mahasiswa Sastra Indonesia UNS Raih Juara 2 Lomba Menulis Cerpen</a></h2>
                                    <div class="category">
                                        <a href="https://fib.uns.ac.id/news/category/kegiatan">Kegiatan</a>
                                    </div>
                                </div>
                            </div>
                                                    <div class="article-item">
                                <div class="article-media">
                                    <img src="https://fib.uns.ac.id/storage/posts/July2020/MWzXdKqxbx4n8EsaVi9Q.jpg" alt="news-fib">
                                </div>

                                <div class="article-title">
                                    <div class="article-meta">
                                        <span>15</span>
                                        <span>APR</span>
                                    </div>
                                    <h2><a href="https://fib.uns.ac.id/news/eratkan-persaudaraan-fib-uns-gelar-silaturahmi-tahunan">Eratkan Persaudaraan, FIB UNS Gelar Silaturahmi Tahunan</a></h2>
                                    <div class="category">
                                        <a href="https://fib.uns.ac.id/news/category/kegiatan">Kegiatan</a>
                                    </div>
                                </div>
                            </div>
                                                    <div class="article-item">
                                <div class="article-media">
                                    <img src="https://fib.uns.ac.id/storage/posts/July2020/xU1OQEmJX96jcFRnL4zx.jpg" alt="news-fib">
                                </div>

                                <div class="article-title">
                                    <div class="article-meta">
                                        <span>15</span>
                                        <span>APR</span>
                                    </div>
                                    <h2><a href="https://fib.uns.ac.id/news/sambut-dies-natalis-ke-44-uns-fib-adakan-festival-pakaian-adat">Sambut Dies Natalis ke-44 UNS, FIB Adakan Festival Pakaian Adat</a></h2>
                                    <div class="category">
                                        <a href="https://fib.uns.ac.id/news/category/kegiatan">Kegiatan</a>
                                    </div>
                                </div>
                            </div>
                                                    <div class="article-item">
                                <div class="article-media">
                                    <img src="https://fib.uns.ac.id/storage/posts/September2020/Yjg1fYuhD5VVE2Bh5bRt.jpeg" alt="news-fib">
                                </div>

                                <div class="article-title">
                                    <div class="article-meta">
                                        <span>07</span>
                                        <span>SEP</span>
                                    </div>
                                    <h2><a href="https://fib.uns.ac.id/news/fib-pitulasan-lomba-tendik-fib-uns-tetap-gunakan-protokol-kesehatan">FIB Pitulasan, Lomba Tendik FIB UNS Tetap Gunakan Protokol Kesehatan</a></h2>
                                    <div class="category">
                                        <a href="https://fib.uns.ac.id/news/category/kegiatan">Kegiatan</a>
                                    </div>
                                </div>
                            </div>
                                            </div>
                </div>
            </div>
        </div>
    </div>
</section>

  <!-- Footer -->
<footer class="bg-footer mt-4 pt-0 pb-2">
  <div class="container">
    <div class="footer-banner mb-5">
      <a href="#"><img src="https://fib.uns.ac.id/img/footer_banner_03.jpg"></a>
    </div>

    <div class="footer-link mt-3">
      <div class="row">
        <div class="col-md-4 pr-0">
          <img src="https://fib.uns.ac.id/img/logo-header-wh.png" alt="logo-fib" class="mb-4">
          <p>
            Fakultas Ilmu Budaya,<br>
            Universitas Sebelas Maret<br>
            Jl. Ir. Sutami No. 36A Kentingan, Jebres,<br>
            Surakarta<br>
            Telp. 0271 635236<br>
            Email : fib@mail.uns.ac.id<br>
          </p>
        </div>
        <div class="col-md-3 pr-0">
          <h5 class="subtitle">Fakultas Ilmu Budaya</h5>
          <ul>
            <li><a href="https://fib.uns.ac.id/page/profile-fakultas">Tentang Fakultas Ilmu Budaya</a></li>
            <li><a href="#">Dosen Fakultas</a></li>
            <li><a href="https://fib.uns.ac.id/news">Berita Terkini</a></li>
            <li><a href="https://fib.uns.ac.id/announcements">Pengumuman</a></li>
            <li><a href="https://fib.uns.ac.id/contact">Kontak Kami</a></li>
          </ul>
        </div>
        <div class="col-md-3 pr-0">
          <h5 class="subtitle">Links</h5>
          <ul>
            <li><a href="#">Penerimaan Mahasiswa Baru</a></li>
            <li><a href="#">Sistem Informasi Akademik</a></li>
            <li><a href="#">Sistem Legalisir FIB</a></li>
            <li><a href="#">Career Development Center</a></li>
            <li><a href="#">Publikasi Ilmiah FIB</a></li>
            <li><a href="#">Portal Dosen</a></li>
          </ul>
        </div>
        <div class="col-md-2 pr-0">
          <h5 class="subtitle">Program Studi</h5>
          <ul>
                                    <li><a href="https://fib.uns.ac.id/prodi/s1-sastra-daerah">S1 Sastra Daerah</a></li>
                        <li><a href="https://fib.uns.ac.id/prodi/s1-sastra-indonesia">S1 Sastra Indonesia</a></li>
                        <li><a href="https://fib.uns.ac.id/prodi/s1-sastra-inggris">S1 Sastra Inggris</a></li>
                        <li><a href="https://fib.uns.ac.id/prodi/s1-ilmu-sejarah">S1 Ilmu Sejarah</a></li>
                        <li><a href="https://fib.uns.ac.id/prodi/s1-sastra-arab">S1 Sastra Arab</a></li>
                        <li><a href="https://fib.uns.ac.id/prodi/s2-linguistik">S2 Linguistik</a></li>
                        <li><a href="https://fib.uns.ac.id/prodi/s3-linguistik">S3 Linguistik</a></li>
                      </ul>
        </div>
      </div> 
    </div>
  </div>

  <div class="copyright text-center mt-5">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="social-link text-left">
            <a href="#" class="text-decoration-none" data-toggle="tooltip" data-placement="top" title="Facebook">
              <img src="https://fib.uns.ac.id/img/facebook.png">
            </a>
            <a href="#" class="text-decoration-none" data-toggle="tooltip" data-placement="top" title="Twitter">
              <img src="https://fib.uns.ac.id/img/twitter.png">
            </a>
            <a href="#" class="text-decoration-none" data-toggle="tooltip" data-placement="top" title="Instagram">
              <img src="https://fib.uns.ac.id/img/instagram.png">
            </a>
          </div>
        </div>
        <div class="col-md-6 text-right">
          Copyright &copy; 2015 - 2020 &nbsp;|&nbsp; All Rights Reserved
        </div>
      </div>
    </div>
  </div>
</footer>
  <!-- Bootstrap core JavaScript -->
<script src="https://fib.uns.ac.id/vendor/jquery/jquery.min.js"></script>
<script src="https://fib.uns.ac.id/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="https://fib.uns.ac.id/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="https://fib.uns.ac.id/vendor/bxslider/jquery.bxslider.min.js"></script>
<script src="https://fib.uns.ac.id/vendor/fancybox/jquery.fancybox.min.js"></script>
<script src="https://fib.uns.ac.id/vendor/owlcarousel/owl.carousel.min.js"></script>

<!-- Custom scripts for this template -->
<script src="https://fib.uns.ac.id/js/app.js"></script>

</body>
</html>