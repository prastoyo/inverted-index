<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US" xml:lang="en-US">
    <head>
        <title>PERTIMBANGAN HAKIM MAHKAMAH AGUNG DALAM MEMUTUS MENGABULKAN PERMOHONAN KASASI PERKARA PERTAMBANGAN DENGAN ADANYA DISSENTING OPINION (STUDI KASUS PUTUSAN MAHKAMAH AGUNG NOMOR 2039 K/PID.SUS.LH/2016) | Al Jundy | Verstek</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="PERTIMBANGAN HAKIM MAHKAMAH AGUNG DALAM MEMUTUS MENGABULKAN PERMOHONAN KASASI PERKARA PERTAMBANGAN DENGAN ADANYA DISSENTING OPINION (STUDI KASUS PUTUSAN MAHKAMAH AGUNG NOMOR 2039 K/PID.SUS.LH/2016)" />
        
        <link rel="icon" href="https://jurnal.uns.ac.id/public/journals/304/journalFavicon_en_US.png" type="image/png" />
        <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/" />

	<meta name="DC.Contributor.Sponsor" xml:lang="en" content="Faculty of Law, Sebelas Maret University"/>
	<meta name="DC.Creator.PersonalName" content="Wildan Al Jundy"/>
	<meta name="DC.Date.created" scheme="ISO8601" content="2020-08-30"/>
	<meta name="DC.Date.dateSubmitted" scheme="ISO8601" content="2020-08-30"/>
	<meta name="DC.Date.issued" scheme="ISO8601" content="2020-08-30"/>
	<meta name="DC.Date.modified" scheme="ISO8601" content="2020-08-30"/>
	<meta name="DC.Description" xml:lang="en" content="ABSTRACT This article aims to know the reason of the Supreme Court Judge’s decision in granting cassation request of mining case with the dissenting opinion is in accordance with Criminal Procedure Code. The research method used is normative legal research. Legal source materials used is primary legal materials and secondary legal materials, legal materials analysis technique used is syllogistic method through deduction thinking patterns or deductive. Based on the results of the research done by the author, it was known that the Supreme Court’s consideration in granting the cassation was in accordance with Criminal Code Procedures (KUHAP). Supreme Court has the right to accept the cassation request and also has the right to adjudicate itself in accordance with Article 254 jo Article 255 section (1) Criminal Procedure Code (KUHAP). Although in deciding the case there is dissenting opinion among the members of the assembly, however the Supreme Court’s decision by taking the most votes is appropriate, this is in accordance with what is stated in Article 182 section 6 letter a Jo Article 256 Criminal Code Procedures (KUHAP). This means that the Supreme Court granted the Cassation by taking the most votes on the difference opinion of the Panel of Judges and cancel the decision of the Sarolangun District Court. Sri Murwahyuni, SH, MH, as a Member Judge has a different opinion, namely the reason that the Public Prosecutor&#039;s Cassation cannot be justified, the judex facti consideration which acquitted the Defendant of the indictment is correct and correct. Key Words: Cassation, Dissenting Opinion, Mining Crimes ABSTRAK Artikel ini bertujuan untuk mengetahui alasan hakim mahkamah agung dalam memutus mengabulkan permohonan kasasi perkara pertambangan dengan adanya dissenting opinion telah sesuai dengan KUHAP. Metode penelitian yang digunakan adalah penelitian hukum normatif. Sumber bahan hukum yang digunakan adalah bahan hukum primer dan bahan hukum sekunder, teknik analisis bahan hukum dengan metode silogisme melalui pola berpikir deduksi atau deduktif. Berdasarkan hasil penelitian yang penulis lakukan, diketahui bahwa pertimbangan Mahkamah Agung dalam mengabulkan kasasi sudah sesuai ketentuan KUHAP. Mahkamah Agung memiliki hak untuk menerima permohonan Kasasi serta memiliki hak untuk mengadili sendiri sesuai dengan Pasal 254 jo Pasal 255 ayat (1) KUHAP. Meskipun dalam memutus perkara terdapat dissenting opinion antar anggota majelis akan tetapi Mahkamah Agung dalam memutus sudah tepat yakni dengan mengambil suara terbanyak, hal ini sudah sesuai dengan yang tertuang dalam Pasal 182 ayat 6 huruf a Jo Pasal 256 KUHAP. berarti bahwa Mahkamah Agung mengabulkan Kasasi dengan mengambil suara terbanyak pada perbedaan pendapat Majelis Hakim dan membatalkan putusan Pengadilan Negeri Sarolangun. Sri Murwahyuni, SH, MH, sebagai Hakim Anggota memiliki pendapat yang berbeda yaitu alasan Kasasi Penuntut Umum tidak dapat dibenarkan, pertimbangan judex facti yang membebaskan Terdakwa dari dakwaan sudah tepat dan benar. Kata Kunci: Kasasi, Dissenting Opinion, Tindak Pidana Pertambangan"/>
	<meta name="DC.Format" scheme="IMT" content="application/pdf"/>
	<meta name="DC.Identifier" content="44102"/>
	<meta name="DC.Identifier.URI" content="https://jurnal.uns.ac.id/verstek/article/view/44102"/>
	<meta name="DC.Language" scheme="ISO639-1" content="ID"/>
	<meta name="DC.Rights" content="Copyright (c) 2020 Verstek" />
	<meta name="DC.Rights" content=""/>
	<meta name="DC.Source" content="Verstek"/>
	<meta name="DC.Source.ISSN" content="2355-0406"/>
	<meta name="DC.Source.Issue" content="2"/>	<meta name="DC.Source.URI" content="https://jurnal.uns.ac.id/verstek"/>
	<meta name="DC.Source.Volume" content="8"/>	<meta name="DC.Title" content="PERTIMBANGAN HAKIM MAHKAMAH AGUNG DALAM MEMUTUS MENGABULKAN PERMOHONAN KASASI PERKARA PERTAMBANGAN DENGAN ADANYA DISSENTING OPINION (STUDI KASUS PUTUSAN MAHKAMAH AGUNG NOMOR 2039 K/PID.SUS.LH/2016)"/>
		<meta name="DC.Type" content="Text.Serial.Journal"/>
	<meta name="DC.Type.articleType" content="Articles"/>
        	<meta name="gs_meta_revision" content="1.1" />
	<meta name="citation_journal_title" content="Verstek"/>
	<meta name="citation_issn" content="2355-0406"/>
        <meta name="citation_author" content="Wildan Al Jundy"/>
        <meta name="citation_author_institution" content="Faculty of Law,
Sebelas Maret University"/>
<meta name="citation_title" content="PERTIMBANGAN HAKIM MAHKAMAH AGUNG DALAM MEMUTUS MENGABULKAN PERMOHONAN KASASI PERKARA PERTAMBANGAN DENGAN ADANYA DISSENTING OPINION (STUDI KASUS PUTUSAN MAHKAMAH AGUNG NOMOR 2039 K/PID.SUS.LH/2016)"/>

	<meta name="citation_date" content="2020/08/30"/>

	<meta name="citation_volume" content="8"/>
	<meta name="citation_issue" content="2"/>

	<meta name="citation_abstract_html_url" content="https://jurnal.uns.ac.id/verstek/article/view/44102"/>
	<meta name="citation_language" content="ID"/>
						<meta name="citation_pdf_url" content="https://jurnal.uns.ac.id/verstek/article/download/44102/pdf"/>
			        

        <link rel="stylesheet" href="https://jurnal.uns.ac.id/lib/pkp/styles/pkp.css" type="text/css" />
<!-- 	<link rel="stylesheet" href="https://jurnal.uns.ac.id/lib/pkp/styles/common.css" type="text/css" />
        <link rel="stylesheet" href="https://jurnal.uns.ac.id/styles/common.css" type="text/css" /> -->
        <link rel="stylesheet" href="https://jurnal.uns.ac.id/styles/compiled.css" type="text/css" />
<!-- 	<link rel="stylesheet" href="https://jurnal.uns.ac.id/styles/articleView.css" type="text/css" /> -->
        
        
        
<!-- 	<link rel="stylesheet" href="https://jurnal.uns.ac.id/styles/sidebar.css" type="text/css" />        <link rel="stylesheet" href="https://jurnal.uns.ac.id/styles/leftSidebar.css" type="text/css" />        <link rel="stylesheet" href="https://jurnal.uns.ac.id/styles/rightSidebar.css" type="text/css" />        <link rel="stylesheet" href="https://jurnal.uns.ac.id/styles/bothSidebars.css" type="text/css" /> -->

        <!-- Base Jquery -->
        <script type="text/javascript" src="http://www.google.com/jsapi"></script>
            <script type="text/javascript">
                // Provide a local fallback if the CDN cannot be reached
                if (typeof google == 'undefined') {
                    document.write(unescape("%3Cscript src='https://jurnal.uns.ac.id/lib/pkp/js/lib/jquery/jquery.min.js' type='text/javascript'%3E%3C/script%3E"));
                    document.write(unescape("%3Cscript src='https://jurnal.uns.ac.id/lib/pkp/js/lib/jquery/plugins/jqueryUi.min.js' type='text/javascript'%3E%3C/script%3E"));
                } else {
                    google.load("jquery", "1.4.4");
                    google.load("jqueryui", "1.8.6");
                }
                </script>
                    
                        <!-- Compiled scripts -->
                                                    
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/lib/jquery/plugins/jquery.tag-it.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/lib/jquery/plugins/jquery.cookie.js"></script>

<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/functions/fontController.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/functions/general.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/functions/jqueryValidatorI18n.js"></script>

<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/classes/Helper.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/classes/ObjectProxy.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/classes/Handler.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/classes/linkAction/LinkActionRequest.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/classes/features/Feature.js"></script>

<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/controllers/SiteHandler.js"></script><!-- Included only for namespace definition -->
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/controllers/UrlInDivHandler.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/controllers/AutocompleteHandler.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/controllers/ExtrasOnDemandHandler.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/controllers/form/FormHandler.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/controllers/form/AjaxFormHandler.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/controllers/form/ClientFormHandler.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/controllers/grid/GridHandler.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/controllers/linkAction/LinkActionHandler.js"></script>

<script type="text/javascript" src="https://cdn.uns.ac.id/common/SearchFormHandler.js"></script>
<script type="text/javascript" src="https://cdn.uns.ac.id/common/ReportGeneratorFormHandler.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/plugins/generic/lucene/js/LuceneAutocompleteHandler.js"></script>

<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/lib/jquery/plugins/jquery.pkp.js"></script>                        
                                                                                                                                
                        <!-- Add View Port -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Add Favicon -->


<!-- Add fonts style sheet -->
<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700|PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<!-- Add theme style sheet -->
<link rel="stylesheet" href="https://jurnal.uns.ac.id/plugins/themes/unsbaru/css/screen.css" type="text/css" />
<link href="https://jurnal.uns.ac.id/plugins/themes/unsbaru/css/print.css" media="print" rel="stylesheet" type="text/css" />

                                                                                    <link rel="stylesheet" href="https://jurnal.uns.ac.id/plugins/generic/doiInSummary/doi.css" type="text/css" />
                                                                                                                <link rel="stylesheet" href="https://jurnal.uns.ac.id/plugins/blocks/languageToggle/styles/languageToggle.css" type="text/css" />
                                                    
                        
	<script type="text/javascript" src="https://cdn.uns.ac.id/common/relatedItems.js"></script>
	<script type="text/javascript" src="https://cdn.uns.ac.id/common/inlinePdf.js"></script>
	<script type="text/javascript" src="https://cdn.uns.ac.id/common/pdfobject.js"></script>

                    </head>
                    <body id="pkp-common-openJournalSystems" class="article">

                        <div id="container">
                            <div id="header">
                                <div id="headerRight">
                                                                            <a href="https://jurnal.uns.ac.id/verstek/login">Login</a>
                                                                                    <a href="https://jurnal.uns.ac.id/verstek/user/register">Register</a>
                                                                                                                </div>
                                    <div id="headerTitle">
                                        <div id="jurnalLogo">
                                                                                    </div>
                                        <div id="jurnalTitle">
                                                                                            <figure>
                                                    <img src="https://jurnal.uns.ac.id/public/journals/304/pageHeaderTitleImage_en_US.png" alt="Page Header" />
                                                </figure>
                                                                                    </div>
                                    </div>
                                    <div style="clear: both"></div>
                                </div>

                                
<nav>

    <div id="fullNav">
        <div id="navbar" role="navigation">
            <ul class="navMenu menu">
                <li id="home"><a href="https://jurnal.uns.ac.id/verstek/index">Home</a></li>
                <li id="about"><a href="https://jurnal.uns.ac.id/verstek/about">About</a></li>


                                    <li id="categories"><a href="https://jurnal.uns.ac.id/index/search/categories">Categories</a></li>
                                                                                
                    
                        <li id="current"><a href="https://jurnal.uns.ac.id/verstek/issue/current">Current</a></li>
                        <li id="archives"><a href="https://jurnal.uns.ac.id/verstek/issue/archive">Archives</a></li>
                        
                                            <li id="announcements"><a href="https://jurnal.uns.ac.id/verstek/announcement">Announcements</a></li>
                                                <li><a href="https://jurnal.uns.ac.id/verstek/statistics" target="_parent">Statistics</a></li>

                                                                                
                                                    <li id="login"><a href="https://jurnal.uns.ac.id/verstek/login">Login</a></li>
                                                                <li id="register"><a href="https://jurnal.uns.ac.id/verstek/user/register">Register</a></li>
                                                                                    </ul>

                                            </div> <!-- End Full Nav -->
                </div>

                <div id="mobileNav">
                    <div id="toggle-bar">
                        <a class="navicon mtoggle" href="#">MAIN MENU</a>
                    </div>
                    <div id="navbar" role="navigation">
                        <ul class="navMenu menu">
                            <li id="home"><a href="https://jurnal.uns.ac.id/verstek/index">Home</a></li>
                            <li id="about"><a href="https://jurnal.uns.ac.id/verstek/about">About</a></li>

                                                                                                                                                                                                                                
                                                            <li id="categories"><a href="https://jurnal.uns.ac.id/index/search/categories">Categories</a></li>
                                                                                                    <li id="search"><a href="https://jurnal.uns.ac.id/verstek/search">Search</a></li>
                                    
                                                                    <li id="current"><a href="https://jurnal.uns.ac.id/verstek/issue/current">Current</a></li>
                                    <li id="archives"><a href="https://jurnal.uns.ac.id/verstek/issue/archive">Archive</a></li>
                                    
                                                                    <li id="announcements"><a href="https://jurnal.uns.ac.id/verstek/announcement">Announcements</a></li>
                                                                        <li id="submitButton"><a href="https://jurnal.uns.ac.id/verstek/about/submissions">Submit</a></li>

                                    <li><a href="https://jurnal.uns.ac.id/verstek/statistics" target="_parent">Statistics</a></li>

                                                                                                                                                        <li>
<div id="searchbar">
<form id="simpleSearchForm" method="post" action="https://jurnal.uns.ac.id/verstek/search/search">							
	<input name="search box" type="search" aria-label="Search" value="" class="textField" />
	<input type="submit" value="Search" class="button" />
</form>
</div></li>
                                </ul>
                            </div>

                        </div>
                    </nav>                                <div id="body">
                                                                                                                        <div id="rightSidebar" class="slide" role="complementary">
                                                                                                    <div class="block" id="issn">
                                                        <span class="blockTitle">ISSN</span>
                                                                                                                                                                                                                                        <p>2355-0406 (Online)</p>
                                                                                                            </div>
                                                                                                <div class="block" id="sidebarDevelopedBy">
	<a class="blockTitle" href="http://pkp.sfu.ca/ojs/" id="developedBy">Open Journal Systems</a>
</div><div class="block" id="sidebarHelp">
	<a class="blockTitle" href="javascript:openHelp('https://jurnal.uns.ac.id/verstek/help')">Journal Help</a>
</div><div class="block" id="sidebarUser">
			<span class="blockTitle">User</span>
	
						<form method="post" action="https://jurnal.uns.ac.id/verstek/login/signIn">
				<table>
					<tr>
						<td><label for="sidebar-username">Username</label></td>
						<td><input type="text" id="sidebar-username" name="username" value="" size="12" maxlength="32" class="textField" /></td>
					</tr>
					<tr>
						<td><label for="sidebar-password">Password</label></td>
						<td><input type="password" id="sidebar-password" name="password" value="" size="12" class="textField" /></td>
					</tr>
					<tr>
						<td colspan="2"><input type="checkbox" id="remember" name="remember" value="1" /> <label for="remember">Remember me</label></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="Login" class="button" /></td>
					</tr>
				</table>
			</form>
			</div><div class="block" id="sidebarNavigation">
	<span class="blockTitle">Journal Content</span>

	<form id="simpleSearchForm" action="https://jurnal.uns.ac.id/verstek/search/search">
		<table id="simpleSearchInput">
			<tr>
				<td>
													<label for="simpleQuery">Search <br />
					<input type="text" id="simpleQuery" name="simpleQuery" size="15" maxlength="255" value="" class="textField" /></label>
								</td>
			</tr>
			<tr>
				<td><label for="searchField">
				Search Scope
				<br />
				<select id="searchField" name="searchField" size="1" class="selectMenu">
					<option label="All" value="query">All</option>
<option label="Authors" value="authors">Authors</option>
<option label="Title" value="title">Title</option>
<option label="Abstract" value="abstract">Abstract</option>
<option label="Index terms" value="indexTerms">Index terms</option>
<option label="Full Text" value="galleyFullText">Full Text</option>

				</select></label>
				</td>
			</tr>
			<tr>
				<td><input type="submit" value="Search" class="button" /></td>
			</tr>
		</table>
	</form>

	<br />

		<span class="blockSubtitle">Browse</span>
	<ul>
		<li><a href="https://jurnal.uns.ac.id/verstek/issue/archive">By Issue</a></li>
		<li><a href="https://jurnal.uns.ac.id/verstek/search/authors">By Author</a></li>
		<li><a href="https://jurnal.uns.ac.id/verstek/search/titles">By Title</a></li>
		
					<li><a href="https://jurnal.uns.ac.id/index">Other Journals</a></li>
			<li><a href="https://jurnal.uns.ac.id/index/search/categories">Categories</a></li>			</ul>
	</div>

<!-- Add javascript required for font sizer -->
<script type="text/javascript">
	<!--
	$(function(){
		fontSize("#sizer", "body", 9, 16, 32, ""); // Initialize the font sizer
	});
	// -->
</script>

<div class="block" id="sidebarFontSize" style="margin-bottom: 4px;">
	<span class="blockTitle">Font Size</span>
	<div id="sizer"></div>
</div>
<br />
                                            </div>
                                                                                                                            <div id="leftSidebar" class="slide" role="complementary">
                                                 <div class="block" id="notification">
	<span class="blockTitle">Notifications</span>
	<ul>
					<li><a href="https://jurnal.uns.ac.id/verstek/notification">View</a></li>
			<li><a href="https://jurnal.uns.ac.id/verstek/notification/subscribeMailList">Subscribe</a></li>
			</ul>
</div>
<div class="block" id="sidebarFlagcounter">
    <span class="blockTitle" >Flagcounter</span>
    <a href="https://s09.flagcounter.com/more/yMdb/" target="_blank"><img style="width: 100%;" src="https://s09.flagcounter.com/map/yMdb/size_t/txt_000000/border_CCCCCC/pageviews_1/viewers_3/flags_0/" alt="Journal Flag Counter" border="0"></a>
</div>
<div class="block" id="sidebarLanguageToggle">
	<script type="text/javascript">
		<!--
		function changeLanguage() {
			var e = document.getElementById('languageSelect');
			var new_locale = e.options[e.selectedIndex].value;

			var redirect_url = 'https://jurnal.uns.ac.id/verstek/user/setLocale/NEW_LOCALE?source=%2Fverstek%2Farticle%2Fview%2F44102%2Fpdf';
			redirect_url = redirect_url.replace("NEW_LOCALE", new_locale);

			window.location.href = redirect_url;
		}
		//-->
	</script>
	<span class="blockTitle">Language</span>
	<form action="#">
		<label for="languageSelect">Select Language</label>
		<select id="languageSelect" size="1" name="locale" class="selectMenu"><option label="Bahasa Indonesia" value="id_ID">Bahasa Indonesia</option>
<option label="English" value="en_US" selected="selected">English</option>
</select>
		<input type="submit" class="button" value="Submit" onclick="changeLanguage(); return false;" />
	</form>
</div>
<div class="block" id="sidebarInformation">
	<span class="blockTitle">Information</span>
	<ul>
		<li><a href="https://jurnal.uns.ac.id/verstek/information/readers">For Readers</a></li>		<li><a href="https://jurnal.uns.ac.id/verstek/information/authors">For Authors</a></li>		<li><a href="https://jurnal.uns.ac.id/verstek/information/librarians">For Librarians</a></li>	</ul>
</div>

                                            </div>
                                                                            

                                                                            <div id="main" role="main" tabindex="-1">
                                        
                                                    <div id="breadcrumb">
                                                        <a href="https://jurnal.uns.ac.id/verstek/index" target="_parent">Home</a> &gt;
                                                        <a href="https://jurnal.uns.ac.id/verstek/issue/view/3117" target="_parent">Vol 8, No 2 (2020)</a> &gt;                                                        <a href="https://jurnal.uns.ac.id/verstek/article/view/44102/pdf" class="current" target="_parent">Al Jundy</a>
                                                    </div>

                                                    <div id="content">


			
<div id="pdfDownloadLinkContainer">
	<a class="action pdf" id="pdfDownloadLink" target="_parent" href="https://jurnal.uns.ac.id/verstek/article/download/44102/pdf">Download this PDF file</a>
</div>



<script type="text/javascript"><!--
	$(document).ready(function(){
		if ($.browser.webkit) { // PDFObject does not correctly work with safari's built-in PDF viewer
			var embedCode = "<object id='pdfObject' type='application/pdf' data='https://jurnal.uns.ac.id/verstek/article/viewFile/44102/pdf'><div id='pluginMissing'><p>The PDF file you selected should load here if your Web browser has a PDF reader plug-in installed (for example, a recent version of <a href=\"https://get.adobe.com/reader/\">Adobe Acrobat Reader<\/a>).<\/p> <p>If you would like more information about how to print, save, and work with PDFs, Highwire Press provides a helpful <a href=\"http://highwire.stanford.edu/help/pdf-faq.dtl\">Frequently Asked Questions about PDFs<\/a>.<\/p> <p>Alternatively, you can download the PDF file directly to your computer, from where it can be opened using a PDF reader. To download the PDF, click the Download link above.<\/p></div></object>";
			$("#inlinePdf").html(embedCode);
			if($("#pluginMissing").is(":hidden")) {
				$('#fullscreenShow').show();
				//$("#inlinePdf").resizable({ containment: 'parent', handles: 'se' });
			} else { // Chrome Mac hides the embed object, obscuring the text.  Reinsert.
				$("#inlinePdf").html('<div id="pluginMissing"><p>The PDF file you selected should load here if your Web browser has a PDF reader plug-in installed (for example, a recent version of <a href=\"https://get.adobe.com/reader/\">Adobe Acrobat Reader<\/a>).<\/p> <p>If you would like more information about how to print, save, and work with PDFs, Highwire Press provides a helpful <a href=\"http://highwire.stanford.edu/help/pdf-faq.dtl\">Frequently Asked Questions about PDFs<\/a>.<\/p> <p>Alternatively, you can download the PDF file directly to your computer, from where it can be opened using a PDF reader. To download the PDF, click the Download link above.<\/p></div>');
			}
		} else {
			var success = new PDFObject({ url: "https://jurnal.uns.ac.id/verstek/article/viewFile/44102/pdf" }).embed("inlinePdf");
			if (success) {
				// PDF was embedded; enbale fullscreen mode and the resizable widget
				$('#fullscreenShow').show();
				$("#inlinePdfResizer").resizable({ containment: 'parent', handles: 'se' });
			}
		}
	});

// -->
</script>
<div id="inlinePdfResizer">
	<div id="inlinePdf" class="ui-widget-content">
		<div id='pluginMissing'><p>The PDF file you selected should load here if your Web browser has a PDF reader plug-in installed (for example, a recent version of <a href="https://get.adobe.com/reader/">Adobe Acrobat Reader</a>).</p> <p>If you would like more information about how to print, save, and work with PDFs, Highwire Press provides a helpful <a href="http://highwire.stanford.edu/help/pdf-faq.dtl">Frequently Asked Questions about PDFs</a>.</p> <p>Alternatively, you can download the PDF file directly to your computer, from where it can be opened using a PDF reader. To download the PDF, click the Download link above.</p></div>
	</div>
</div>
<p>
	<a class="action" href="#" id="fullscreenShow">Fullscreen</a>
	<a class="action" href="#" id="fullscreenHide">Fullscreen Off</a>
</p>
<div style="clear: both;"></div>	<div class="block">
</div>


	<a class="twitter-share-button" href="https://twitter.com/share" data-text="PERTIMBANGAN HAKIM MAHKAMAH AGUNG DALAM MEMUTUS MENGABULKAN PERMOHONAN KASASI PERKARA PERTAMBANGAN DENGAN ADANYA DISSENTING OPINION (STUDI KASUS PUTUSAN MAHKAMAH AGUNG NOMOR 2039 K/PID.SUS.LH/2016)" data-size="large">Tweet</a>


<div class="block">

<div class="separator"></div>

<h3>Refbacks</h3>

<ul>
				<li>There are currently no refbacks.</li>
	</ul></div>



</div><!-- content -->
<div id="foot"></div></div><!-- main --></div><!-- body --><div id="pageFooter"><div id="standardFooter"><div id="right"><p><font color=#FFFFFF>Alamat </font></p><p><font color=#FFFFFF>Jalan Ir. Sutami 36 A, Surakarta, 57126 </font></p><p><font color=#FFFFFF>(0271) 638959 </font></p></div><div id="center"><p><font color="white">Copyright &copy;  2017 <a href="https://uns.ac.id">Universitas Sebelas Maret</a></font></p></div><div id="left"><a href="https://uns.ac.id"><img src="https://jurnal.uns.ac.id/plugins/themes/unsbaru/img/logouns.png" alt="Universitas Sebelas Maret Logo"/></a></div><div style="clear: both"></div></div>
<script type="text/javascript" src="https://jurnal.uns.ac.id/plugins/themes/unsbaru/js/menu.js"></script>
</div>
<script type="text/javascript" src="https://jurnal.uns.ac.id/plugins/themes/mpg/js/menu.js"></script>
<script type='text/javascript' src='https://d1bxh8uas1mnw7.cloudfront.net/assets/embed.js'></script>

    <div  id="listFoot"  style="clear: both"></div>
</div> <!-- container -->



<script type="text/javascript">
    
        window.twttr = (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0],
                    t = window.twttr || {};
            if (d.getElementById(id))
                return t;
            js = d.createElement(s);
            js.id = id;
            js.src = "https://platform.twitter.com/widgets.js";
            fjs.parentNode.insertBefore(js, fjs);

            t._e = [];
            t.ready = function (f) {
                t._e.push(f);
            };

            return t;
        }(document, "script", "twitter-wjs"));
    
</script>


<script type="text/javascript">
    
        $(document).ready(function () {

            $('#userHome').hide();
            $('#login').hide();
            $('#register').hide();
            //change the integers below to match the height of your upper dive, which I called
            //banner.  Just add a 1 to the last number.  console.log($(window).scrollTop())
            //to figure out what the scroll position is when exactly you want to fix the nav
            //bar or div or whatever.  I stuck in the console.log for you.  Just remove when
            //you know the position.
            $(window).scroll(function () {

                console.log($(window).scrollTop());
                if ($(window).scrollTop() > $('#headerTitle').height() - 38) {
                    $('nav').addClass('navbar-fixed');
                    $('#userHome').show();
                    $('#login').show();
                    $('#register').show();
                }

                if ($(window).scrollTop() < $('#headerTitle').height() - 37) {
                    $('nav').removeClass('navbar-fixed');
                    $('#userHome').hide();
                    $('#login').hide();
                    $('#register').hide();
                }
            });
        });
    
</script>
</body>
</html>