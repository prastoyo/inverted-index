<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US" xml:lang="en-US">
    <head>
        <title>Baster Pada Penutur Bilingual Jawa-Indonesia | Wijayanti | Prosiding Seminar Nasional Linguistik dan Sastra (SEMANTIKS)</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="Baster Pada Penutur Bilingual Jawa-Indonesia" />
                    <meta name="keywords" content="baster, campur kode; bilingualisme; penutur Jawa-Indonesia" />
        
        
        <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/" />

	<meta name="DC.Contributor.Sponsor" xml:lang="en" content=""/>
	<meta name="DC.Creator.PersonalName" content="Kenfitria Diah Wijayanti"/>
	<meta name="DC.Date.created" scheme="ISO8601" content="2019-12-30"/>
	<meta name="DC.Date.dateSubmitted" scheme="ISO8601" content="2020-02-09"/>
	<meta name="DC.Date.issued" scheme="ISO8601" content="2019-12-30"/>
	<meta name="DC.Date.modified" scheme="ISO8601" content="2020-02-09"/>
	<meta name="DC.Description" xml:lang="en" content="   Abstract:  Javanese speakers have the ability to master more than one language. The second language that is mastered by Javanese is Indonesian. In its use often occurs mixing of the two languages. But the mixing that occurs is an imposing form. The coercion of the two languages can be categorized as a baster. Baster is a form of code mix variation. Baster is a meaningful blend of two languages. The mixture often appears accidentally by the speaker. The writing of this article aims to: (1) describe the form of baster that occurs in Javanese-Indonesian bilingual speakers, (2) describe the factors affecting the occurrence of baster in Javanese-Indonesian bilingual speakers. This research is descriptive qualitative. The data used in this study are in the form of words, phrases, clauses containing Javanese-Indonesian baster. The sampling technique is done by purposive sampling. Provision of data in this study uses the refer method. The basic technique is tapping, while the advanced technique is note taking. Data analysis using the matching method. The sociolinguistic approach is used to study the data found.    Abstrak:  Penutur Jawa memiliki kemampuan menguasai lebih dari satu bahasa. Bahasa kedua yang dikuasai masyarakat Jawa adalah bahasa Indonesia. Dalam pemakaiannya seringkali terjadi percampuran kedua bahasa tersebut. Namun percampuran yang terjadi adalah bentuk yang memaksakan. Pemaksaan bercampurnya dua bahasa tersebut dapat dikategorikan dalam baster. Baster merupakan salah satu bentuk variasi campur kode. Baster adalah perpaduan dua bahasa yang bermakna. Percampuran tersebut sering muncul tanpa disengaja oleh penuturnya. Penulisan artikel ini bertujuan untuk: (1)mendeskripsikan bentuk baster yang terjadi pada penutur bilingual Jawa-Indonesia, (2)mendeskripsikan faktor pemengaruh terjadinya baster pada penutur bilingual Jawa-Indonesia. Penelitian ini bersifat deskriptif kualitatif. Data yang digunakan dalam penelitian ini berupa kata, frasa, klausa yang mengandung baster Jawa-Indonesia. Teknik pengambilan sampel dilakukan dengan  purposive sampling.  Penyediaan data dalam penelitian ini menggunakan metode simak. Teknik dasarnya adalah teknik sadap, sedangkan teknik lanjutanya adalah teknik catat. Analisis data dengan menggunakan metode padan. Pendekatan sosiolinguistik digunakan untuk mengkaji data yang ditemukan. "/>
	<meta name="DC.Format" scheme="IMT" content="application/pdf"/>
	<meta name="DC.Identifier" content="39786"/>
	<meta name="DC.Identifier.pageNumber" content="732-740"/>
	<meta name="DC.Identifier.URI" content="https://jurnal.uns.ac.id/prosidingsemantiks/article/view/39786"/>
	<meta name="DC.Language" scheme="ISO639-1" content="id"/>
	<meta name="DC.Rights" content="Copyright (c) 2020 Prosiding Seminar Nasional Linguistik dan Sastra (SEMANTIKS)" />
	<meta name="DC.Rights" content=""/>
	<meta name="DC.Source" content="Prosiding Seminar Nasional Linguistik dan Sastra (SEMANTIKS)"/>
	<meta name="DC.Source.Issue" content="0"/>	<meta name="DC.Source.URI" content="https://jurnal.uns.ac.id/prosidingsemantiks"/>
	<meta name="DC.Source.Volume" content="1"/>						<meta name="DC.Subject" xml:lang="en" content="baster, campur kode"/>
								<meta name="DC.Subject" xml:lang="en" content="bilingualisme"/>
								<meta name="DC.Subject" xml:lang="en" content="penutur Jawa-Indonesia"/>
				<meta name="DC.Title" content="Baster Pada Penutur Bilingual Jawa-Indonesia"/>
		<meta name="DC.Type" content="Text.Serial.Journal"/>
	<meta name="DC.Type.articleType" content="Articles"/>
        	<meta name="gs_meta_revision" content="1.1" />
	<meta name="citation_journal_title" content="Prosiding Seminar Nasional Linguistik dan Sastra (SEMANTIKS)"/>
        <meta name="citation_author" content="Kenfitria Diah Wijayanti"/>
        <meta name="citation_author_institution" content="Program Studi S3 Linguistik, Pascasarjana Universitas Sebelas Maret,
Jl. Ir. Sutami no 36 Kentingan Surakarta"/>
<meta name="citation_title" content="Baster Pada Penutur Bilingual Jawa-Indonesia"/>

	<meta name="citation_date" content="2019/12/30"/>

	<meta name="citation_volume" content="1"/>
	<meta name="citation_issue" content="0"/>

			<meta name="citation_firstpage" content="732"/>
				<meta name="citation_lastpage" content="740"/>
		<meta name="citation_abstract_html_url" content="https://jurnal.uns.ac.id/prosidingsemantiks/article/view/39786"/>
	<meta name="citation_language" content="id"/>
						<meta name="citation_keywords" xml:lang="en" content="baster, campur kode"/>
								<meta name="citation_keywords" xml:lang="en" content="bilingualisme"/>
								<meta name="citation_keywords" xml:lang="en" content="penutur Jawa-Indonesia"/>
									<meta name="citation_pdf_url" content="https://jurnal.uns.ac.id/prosidingsemantiks/article/download/39786/26191"/>
			        

        <link rel="stylesheet" href="https://jurnal.uns.ac.id/lib/pkp/styles/pkp.css" type="text/css" />
<!-- 	<link rel="stylesheet" href="https://jurnal.uns.ac.id/lib/pkp/styles/common.css" type="text/css" />
        <link rel="stylesheet" href="https://jurnal.uns.ac.id/styles/common.css" type="text/css" /> -->
        <link rel="stylesheet" href="https://jurnal.uns.ac.id/styles/compiled.css" type="text/css" />
<!-- 	<link rel="stylesheet" href="https://jurnal.uns.ac.id/styles/articleView.css" type="text/css" /> -->
                    <link rel="stylesheet" href="https://jurnal.uns.ac.id/lib/pkp/styles/rtEmbedded.css" type="text/css" />
        
        
        
<!-- 	<link rel="stylesheet" href="https://jurnal.uns.ac.id/styles/sidebar.css" type="text/css" />                <link rel="stylesheet" href="https://jurnal.uns.ac.id/styles/rightSidebar.css" type="text/css" />         -->

        <!-- Base Jquery -->
        <script type="text/javascript" src="http://www.google.com/jsapi"></script>
            <script type="text/javascript">
                // Provide a local fallback if the CDN cannot be reached
                if (typeof google == 'undefined') {
                    document.write(unescape("%3Cscript src='https://jurnal.uns.ac.id/lib/pkp/js/lib/jquery/jquery.min.js' type='text/javascript'%3E%3C/script%3E"));
                    document.write(unescape("%3Cscript src='https://jurnal.uns.ac.id/lib/pkp/js/lib/jquery/plugins/jqueryUi.min.js' type='text/javascript'%3E%3C/script%3E"));
                } else {
                    google.load("jquery", "1.4.4");
                    google.load("jqueryui", "1.8.6");
                }
                </script>
                    
                        <!-- Compiled scripts -->
                                                    
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/lib/jquery/plugins/jquery.tag-it.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/lib/jquery/plugins/jquery.cookie.js"></script>

<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/functions/fontController.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/functions/general.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/functions/jqueryValidatorI18n.js"></script>

<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/classes/Helper.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/classes/ObjectProxy.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/classes/Handler.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/classes/linkAction/LinkActionRequest.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/classes/features/Feature.js"></script>

<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/controllers/SiteHandler.js"></script><!-- Included only for namespace definition -->
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/controllers/UrlInDivHandler.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/controllers/AutocompleteHandler.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/controllers/ExtrasOnDemandHandler.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/controllers/form/FormHandler.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/controllers/form/AjaxFormHandler.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/controllers/form/ClientFormHandler.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/controllers/grid/GridHandler.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/controllers/linkAction/LinkActionHandler.js"></script>

<script type="text/javascript" src="https://cdn.uns.ac.id/common/SearchFormHandler.js"></script>
<script type="text/javascript" src="https://cdn.uns.ac.id/common/ReportGeneratorFormHandler.js"></script>
<script type="text/javascript" src="https://jurnal.uns.ac.id/plugins/generic/lucene/js/LuceneAutocompleteHandler.js"></script>

<script type="text/javascript" src="https://jurnal.uns.ac.id/lib/pkp/js/lib/jquery/plugins/jquery.pkp.js"></script>                        
                                                                                                                                
                        <!-- Add View Port -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Add Favicon -->


<!-- Add fonts style sheet -->
<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700|PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<!-- Add theme style sheet -->
<link rel="stylesheet" href="https://jurnal.uns.ac.id/plugins/themes/unsbaru/css/screen.css" type="text/css" />
<link href="https://jurnal.uns.ac.id/plugins/themes/unsbaru/css/print.css" media="print" rel="stylesheet" type="text/css" />

                                                                                    <link rel="stylesheet" href="https://jurnal.uns.ac.id/plugins/generic/doiInSummary/doi.css" type="text/css" />
                                                                                                                <link rel="stylesheet" href="https://jurnal.uns.ac.id/plugins/blocks/languageToggle/styles/languageToggle.css" type="text/css" />
                                                    
                        
	<script type="text/javascript" src="https://cdn.uns.ac.id/common/relatedItems.js"></script>
	<script type="text/javascript" src="https://cdn.uns.ac.id/common/inlinePdf.js"></script>
	<script type="text/javascript" src="https://cdn.uns.ac.id/common/pdfobject.js"></script>

                    </head>
                    <body id="pkp-common-openJournalSystems" class="article">

                        <div id="container">
                            <div id="header">
                                <div id="headerRight">
                                                                            <a href="https://jurnal.uns.ac.id/prosidingsemantiks/login">Login</a>
                                                                                    <a href="https://jurnal.uns.ac.id/prosidingsemantiks/user/register">Register</a>
                                                                                                                </div>
                                    <div id="headerTitle">
                                        <div id="jurnalLogo">
                                                                                    </div>
                                        <div id="jurnalTitle">
                                                                                            <figure>
                                                    <img src="https://jurnal.uns.ac.id/public/journals/356/pageHeaderTitleImage_en_US.png" alt="Page Header" />
                                                </figure>
                                                                                    </div>
                                    </div>
                                    <div style="clear: both"></div>
                                </div>

                                
<nav>

    <div id="fullNav">
        <div id="navbar" role="navigation">
            <ul class="navMenu menu">
                <li id="home"><a href="https://jurnal.uns.ac.id/prosidingsemantiks/index">Home</a></li>
                <li id="about"><a href="https://jurnal.uns.ac.id/prosidingsemantiks/about">About</a></li>


                                    <li id="categories"><a href="https://jurnal.uns.ac.id/index/search/categories">Categories</a></li>
                                                                                
                    
                        <li id="current"><a href="https://jurnal.uns.ac.id/prosidingsemantiks/issue/current">Current</a></li>
                        <li id="archives"><a href="https://jurnal.uns.ac.id/prosidingsemantiks/issue/archive">Archives</a></li>
                        
                                            <li><a href="https://jurnal.uns.ac.id/prosidingsemantiks/statistics" target="_parent">Statistics</a></li>

                                                                                
                                                    <li id="login"><a href="https://jurnal.uns.ac.id/prosidingsemantiks/login">Login</a></li>
                                                                <li id="register"><a href="https://jurnal.uns.ac.id/prosidingsemantiks/user/register">Register</a></li>
                                                                                    </ul>

                                            </div> <!-- End Full Nav -->
                </div>

                <div id="mobileNav">
                    <div id="toggle-bar">
                        <a class="navicon mtoggle" href="#">MAIN MENU</a>
                    </div>
                    <div id="navbar" role="navigation">
                        <ul class="navMenu menu">
                            <li id="home"><a href="https://jurnal.uns.ac.id/prosidingsemantiks/index">Home</a></li>
                            <li id="about"><a href="https://jurnal.uns.ac.id/prosidingsemantiks/about">About</a></li>

                                                                                                                                                                                                                                
                                                            <li id="categories"><a href="https://jurnal.uns.ac.id/index/search/categories">Categories</a></li>
                                                                                                    <li id="search"><a href="https://jurnal.uns.ac.id/prosidingsemantiks/search">Search</a></li>
                                    
                                                                    <li id="current"><a href="https://jurnal.uns.ac.id/prosidingsemantiks/issue/current">Current</a></li>
                                    <li id="archives"><a href="https://jurnal.uns.ac.id/prosidingsemantiks/issue/archive">Archive</a></li>
                                    
                                                                    <li id="submitButton"><a href="https://jurnal.uns.ac.id/prosidingsemantiks/about/submissions">Submit</a></li>

                                    <li><a href="https://jurnal.uns.ac.id/prosidingsemantiks/statistics" target="_parent">Statistics</a></li>

                                                                                                                                                        <li>
<div id="searchbar">
<form id="simpleSearchForm" method="post" action="https://jurnal.uns.ac.id/prosidingsemantiks/search/search">							
	<input name="search box" type="search" aria-label="Search" value="" class="textField" />
	<input type="submit" value="Search" class="button" />
</form>
</div></li>
                                </ul>
                            </div>

                        </div>
                    </nav>                                <div id="body">
                                                                                                                        <div id="rightSidebar" class="slide" role="complementary">
                                                                                                <div class="block" id="sidebarDevelopedBy">
	<a class="blockTitle" href="http://pkp.sfu.ca/ojs/" id="developedBy">Open Journal Systems</a>
</div><div class="block" id="sidebarHelp">
	<a class="blockTitle" href="javascript:openHelp('https://jurnal.uns.ac.id/prosidingsemantiks/help')">Journal Help</a>
</div><div class="block" id="sidebarUser">
			<span class="blockTitle">User</span>
	
						<form method="post" action="https://jurnal.uns.ac.id/prosidingsemantiks/login/signIn">
				<table>
					<tr>
						<td><label for="sidebar-username">Username</label></td>
						<td><input type="text" id="sidebar-username" name="username" value="" size="12" maxlength="32" class="textField" /></td>
					</tr>
					<tr>
						<td><label for="sidebar-password">Password</label></td>
						<td><input type="password" id="sidebar-password" name="password" value="" size="12" class="textField" /></td>
					</tr>
					<tr>
						<td colspan="2"><input type="checkbox" id="remember" name="remember" value="1" /> <label for="remember">Remember me</label></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="Login" class="button" /></td>
					</tr>
				</table>
			</form>
			</div>
<div class="block" id="sidebarRTAuthorBios">
	<span class="blockTitle">
					About The Author
			</span>
		<div class="authorBio">
	<p>
		<em>Kenfitria Diah Wijayanti</em><br />
								Program Studi S3 Linguistik, Pascasarjana Universitas Sebelas Maret,
Jl. Ir. Sutami no 36 Kentingan Surakarta		<br/>Indonesia	</p>

	<p></p>
	</div>
	
	</div><div class="block" id="sidebarFlagcounter">
    <span class="blockTitle" >Flagcounter</span>
    <a href="https://s09.flagcounter.com/more/yMdb/" target="_blank"><img style="width: 100%;" src="https://s09.flagcounter.com/map/yMdb/size_t/txt_000000/border_CCCCCC/pageviews_1/viewers_3/flags_0/" alt="Journal Flag Counter" border="0"></a>
</div>
<!-- Add javascript required for font sizer -->
<script type="text/javascript">
	<!--
	$(function(){
		fontSize("#sizer", "body", 9, 16, 32, ""); // Initialize the font sizer
	});
	// -->
</script>

<div class="block" id="sidebarFontSize" style="margin-bottom: 4px;">
	<span class="blockTitle">Font Size</span>
	<div id="sizer"></div>
</div>
<br /><div class="block custom" id="customblock-INDEXING">
	<p><span style="font-size: medium;"><a style="background: #282828 none repeat scroll 0% 0%; border-bottom: 1px solid #c10303; color: white; display: block; padding: 0.66em 10px 0.6em 13px;">Indexed by</a></span><strong></strong></p>
<p><a href="https://scholar.google.co.id/scholar?hl=en&amp;as_sdt=0%2C5&amp;q=Prosiding+Seminar+Nasional+Linguistik+dan+Sastra+%28SEMANTIKS%29&amp;btnG=" target="_ blank"><img src="https://scholar.google.co.id/intl/en/scholar/images/1x/googlelogo_color_270x104dp.png" alt="Google Scholar" height="40" border="no" /></a></p>
</div>	 <div class="block" id="notification">
	<span class="blockTitle">Notifications</span>
	<ul>
					<li><a href="https://jurnal.uns.ac.id/prosidingsemantiks/notification">View</a></li>
			<li><a href="https://jurnal.uns.ac.id/prosidingsemantiks/notification/subscribeMailList">Subscribe</a></li>
			</ul>
</div>

<div class="block" id="sidebarLanguageToggle">
	<script type="text/javascript">
		<!--
		function changeLanguage() {
			var e = document.getElementById('languageSelect');
			var new_locale = e.options[e.selectedIndex].value;

			var redirect_url = 'https://jurnal.uns.ac.id/prosidingsemantiks/user/setLocale/NEW_LOCALE?source=%2Fprosidingsemantiks%2Farticle%2Fview%2F39786';
			redirect_url = redirect_url.replace("NEW_LOCALE", new_locale);

			window.location.href = redirect_url;
		}
		//-->
	</script>
	<span class="blockTitle">Language</span>
	<form action="#">
		<label for="languageSelect">Select Language</label>
		<select id="languageSelect" size="1" name="locale" class="selectMenu"><option label="Bahasa Indonesia" value="id_ID">Bahasa Indonesia</option>
<option label="English" value="en_US" selected="selected">English</option>
</select>
		<input type="submit" class="button" value="Submit" onclick="changeLanguage(); return false;" />
	</form>
</div>
<div class="block" id="sidebarNavigation">
	<span class="blockTitle">Journal Content</span>

	<form id="simpleSearchForm" action="https://jurnal.uns.ac.id/prosidingsemantiks/search/search">
		<table id="simpleSearchInput">
			<tr>
				<td>
													<label for="simpleQuery">Search <br />
					<input type="text" id="simpleQuery" name="simpleQuery" size="15" maxlength="255" value="" class="textField" /></label>
								</td>
			</tr>
			<tr>
				<td><label for="searchField">
				Search Scope
				<br />
				<select id="searchField" name="searchField" size="1" class="selectMenu">
					<option label="All" value="query">All</option>
<option label="Authors" value="authors">Authors</option>
<option label="Title" value="title">Title</option>
<option label="Abstract" value="abstract">Abstract</option>
<option label="Index terms" value="indexTerms">Index terms</option>
<option label="Full Text" value="galleyFullText">Full Text</option>

				</select></label>
				</td>
			</tr>
			<tr>
				<td><input type="submit" value="Search" class="button" /></td>
			</tr>
		</table>
	</form>

	<br />

		<span class="blockSubtitle">Browse</span>
	<ul>
		<li><a href="https://jurnal.uns.ac.id/prosidingsemantiks/issue/archive">By Issue</a></li>
		<li><a href="https://jurnal.uns.ac.id/prosidingsemantiks/search/authors">By Author</a></li>
		<li><a href="https://jurnal.uns.ac.id/prosidingsemantiks/search/titles">By Title</a></li>
		
					<li><a href="https://jurnal.uns.ac.id/index">Other Journals</a></li>
			<li><a href="https://jurnal.uns.ac.id/index/search/categories">Categories</a></li>			</ul>
	</div>
<div class="block" id="sidebarInformation">
	<span class="blockTitle">Information</span>
	<ul>
		<li><a href="https://jurnal.uns.ac.id/prosidingsemantiks/information/readers">For Readers</a></li>		<li><a href="https://jurnal.uns.ac.id/prosidingsemantiks/information/authors">For Authors</a></li>		<li><a href="https://jurnal.uns.ac.id/prosidingsemantiks/information/librarians">For Librarians</a></li>	</ul>
</div>

                                            </div>
                                                                                                                    

                                                                                                                                    <div id="main" style="width:78%;" role="main" tabindex="-1">
                                                                                                        
                                                    <div id="breadcrumb">
                                                        <a href="https://jurnal.uns.ac.id/prosidingsemantiks/index" target="_parent">Home</a> &gt;
                                                        <a href="https://jurnal.uns.ac.id/prosidingsemantiks/issue/view/2885" target="_parent">2019</a> &gt;                                                        <a href="https://jurnal.uns.ac.id/prosidingsemantiks/article/view/39786/0" class="current" target="_parent">Wijayanti</a>
                                                    </div>

                                                    <div id="content">



	<div id="topBar">
					</div>
		
			<div data-badge-popover="bottom" data-badge-type="donut" data-hide-no-mentions="true" data-doi="" class="altmetric-embed right" ></div>
		<div id="articleTitle" class="inline-block"><h3>Baster Pada Penutur Bilingual Jawa-Indonesia</h3>
	<div id="authorString"><em>Kenfitria Diah Wijayanti</em></div>
	</div>
			<div id="articleAbstract" class="block">
		<h4>Abstract</h4>
		<div><p><em><strong>Abstract:</strong> Javanese speakers have the ability to master more than one language. The second language that is mastered by Javanese is Indonesian. In its use often occurs mixing of the two languages. But the mixing that occurs is an imposing form. The coercion of the two languages can be categorized as a baster. Baster is a form of code mix variation. Baster is a meaningful blend of two languages. The mixture often appears accidentally by the speaker. The writing of this article aims to: (1) describe the form of baster that occurs in Javanese-Indonesian bilingual speakers, (2) describe the factors affecting the occurrence of baster in Javanese-Indonesian bilingual speakers. This research is descriptive qualitative. The data used in this study are in the form of words, phrases, clauses containing Javanese-Indonesian baster. The sampling technique is done by purposive sampling. Provision of data in this study uses the refer method. The basic technique is tapping, while the advanced technique is note taking. Data analysis using the matching method. The sociolinguistic approach is used to study the data found.</em></p><p><strong>Abstrak:</strong> Penutur Jawa memiliki kemampuan menguasai lebih dari satu bahasa. Bahasa kedua yang dikuasai masyarakat Jawa adalah bahasa Indonesia. Dalam pemakaiannya seringkali terjadi percampuran kedua bahasa tersebut. Namun percampuran yang terjadi adalah bentuk yang memaksakan. Pemaksaan bercampurnya dua bahasa tersebut dapat dikategorikan dalam baster. Baster merupakan salah satu bentuk variasi campur kode. Baster adalah perpaduan dua bahasa yang bermakna. Percampuran tersebut sering muncul tanpa disengaja oleh penuturnya. Penulisan artikel ini bertujuan untuk: (1)mendeskripsikan bentuk baster yang terjadi pada penutur bilingual Jawa-Indonesia, (2)mendeskripsikan faktor pemengaruh terjadinya baster pada penutur bilingual Jawa-Indonesia. Penelitian ini bersifat deskriptif kualitatif. Data yang digunakan dalam penelitian ini berupa kata, frasa, klausa yang mengandung baster Jawa-Indonesia. Teknik pengambilan sampel dilakukan dengan <em>purposive sampling. </em>Penyediaan data dalam penelitian ini menggunakan metode simak. Teknik dasarnya adalah teknik sadap, sedangkan teknik lanjutanya adalah teknik catat. Analisis data dengan menggunakan metode padan. Pendekatan sosiolinguistik digunakan untuk mengkaji data yang ditemukan.</p></div>
		</div>
	
			<div id="articleSubject" class="block">
		<h4>Keywords</h4>
		<div>baster, campur kode; bilingualisme; penutur Jawa-Indonesia</div>
		</div>
	
				
			<div id="articleFullText" class="block">
		<h4>Full Text:</h4>
									<a href="https://jurnal.uns.ac.id/prosidingsemantiks/article/view/39786/26191" class="file" target="_parent">PDF</a>
														</div>
	
			<div id="articleCitations" class="block">
		<h4>References</h4>
		<div>
							<p>Ansar, Fithrah A. 2017. Code Swicthing and Code Mixing in Teaching Learning Process. Jurnal Tadris Bahasa Inggris. Vol 10 (1), 29-45. Retrievied from http://ejournal.ac.id/index.php/ENGEDU.</p>
							<p>Chaer, Abdul dan Agustina, Leonie. 2004. Sosiolinguistik Perkenalan Awal. Jakarta: Rineka Cipta.</p>
							<p>Duran, L. (1994). Toward a Better Understanding of Code Switching and Interlanguage in Bilinguality: Implication for Bilingual Instruction. Retrieved from http://www.ncela.gwu.edu/pubs/jeilms/vol14/duran.htm.</p>
							<p>Husin, MS &amp; Arifin, K. 2011. Code-Switching and Code-Mixing of English and Bahasa Malaysia in Content-Based Classrooms: Frequency and Attitudes. The Linguistics Journal Vol 5 Issue 1.</p>
							<p>Kridalaksana, Harimurti. 2011. Kamus Linguistik Edisi ke Empat. Jakarta: PT. Gramedia Pustaka Utama.</p>
							<p>Pateda, Mansoer. 1987. Sosiolinguistik. Bandung: Angkasa.</p>
							<p>Poedjasoedarma, Soepomo, dkk. 1979. Tingkat Tutur Bahasa Jawa. Jakarta: Pusat Pembinaan dan Pengembangan Bahasa, Departemen Pendidikan dan Kebudayaan.</p>
							<p>Pranowo. 2014. Teori Belajar Bahasa. Yogyakarta: Pustaka Pelajar.</p>
							<p>Rahadi, Kunjana. 2001. Sosiolinguistik Kode dan Alih Kode. Yogyakarta: Pustaka Pelajar.</p>
							<p>Sasangka, Sri Satriya Tjatur Wisnu. 2007. Unggah-ungguh Basa Jawa. Jakarta: Yayasan Paramalingua.</p>
							<p>Sudaryanto. 2015. Metode dan Aneka Teknik Analisis Bahasa. Yogyakarta: Sanata Dharma Univesity Press.</p>
							<p>Sumarsono. 2014. Sosiolinguistik. Yogyakarta: Pustaka Pelajar.</p>
							<p>Suwito. 1983. Pengantar Awal Sosiolinguistik Teori dan Problema. Surakarta: Henary Offset Solo.</p>
							<p>Yule, George. 2015. Kajian Bahasa. Yogyakarta: Pustaka Pelajar.</p>
					</div>
		</div>
	<div class="block">
</div>


	<a class="twitter-share-button" href="https://twitter.com/share" data-text="Baster Pada Penutur Bilingual Jawa-Indonesia" data-size="large">Tweet</a>


<div class="block">

<div class="separator"></div>

<h3>Refbacks</h3>

<ul>
				<li>There are currently no refbacks.</li>
	</ul></div>



</div><!-- content -->
<div id="foot"></div></div><!-- main --></div><!-- body --><div id="pageFooter"><div id="standardFooter"><div id="right"><p><font color=#FFFFFF>Alamat </font></p><p><font color=#FFFFFF>Jalan Ir. Sutami 36 A, Surakarta, 57126 </font></p><p><font color=#FFFFFF>(0271) 638959 </font></p></div><div id="center"><p><font color="white">Copyright &copy;  2017 <a href="https://uns.ac.id">Universitas Sebelas Maret</a></font></p></div><div id="left"><a href="https://uns.ac.id"><img src="https://jurnal.uns.ac.id/plugins/themes/unsbaru/img/logouns.png" alt="Universitas Sebelas Maret Logo"/></a></div><div style="clear: both"></div></div>
<script type="text/javascript" src="https://jurnal.uns.ac.id/plugins/themes/unsbaru/js/menu.js"></script>
</div>
<script type="text/javascript" src="https://jurnal.uns.ac.id/plugins/themes/mpg/js/menu.js"></script>
<script type='text/javascript' src='https://d1bxh8uas1mnw7.cloudfront.net/assets/embed.js'></script>

    <div  id="listFoot"  style="clear: both"></div>
</div> <!-- container -->



<script type="text/javascript">
    
        window.twttr = (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0],
                    t = window.twttr || {};
            if (d.getElementById(id))
                return t;
            js = d.createElement(s);
            js.id = id;
            js.src = "https://platform.twitter.com/widgets.js";
            fjs.parentNode.insertBefore(js, fjs);

            t._e = [];
            t.ready = function (f) {
                t._e.push(f);
            };

            return t;
        }(document, "script", "twitter-wjs"));
    
</script>


<script type="text/javascript">
    
        $(document).ready(function () {

            $('#userHome').hide();
            $('#login').hide();
            $('#register').hide();
            //change the integers below to match the height of your upper dive, which I called
            //banner.  Just add a 1 to the last number.  console.log($(window).scrollTop())
            //to figure out what the scroll position is when exactly you want to fix the nav
            //bar or div or whatever.  I stuck in the console.log for you.  Just remove when
            //you know the position.
            $(window).scroll(function () {

                console.log($(window).scrollTop());
                if ($(window).scrollTop() > $('#headerTitle').height() - 38) {
                    $('nav').addClass('navbar-fixed');
                    $('#userHome').show();
                    $('#login').show();
                    $('#register').show();
                }

                if ($(window).scrollTop() < $('#headerTitle').height() - 37) {
                    $('nav').removeClass('navbar-fixed');
                    $('#userHome').hide();
                    $('#login').hide();
                    $('#register').hide();
                }
            });
        });
    
</script>
</body>
</html>