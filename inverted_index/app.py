from preprocess import CustomTokenizer
from statistics import TfidfRanker, add_page_rank_scores_and_reorder
# import CustomGUI as gui
import pickle
import time
from pseudo_relevance_feedback import CustomPseudoRelevanceFeedback
import webbrowser
import pprint
from flask import Flask, render_template, request

'''
This script is the main program of this project, by running it the user is displayed a GUI where he can choose some
settings and start running his queries and seeing the results
'''
N_PAGES = 10000
RESULTS_PER_PAGE = 10
MAX_RESULTS_TO_CONSIDER = 100

# Default values overridden on application start when the users sets his preferences
USE_PAGE_RANK = False
USE_PAGE_RANK_EARLY = False  # I decided not to let the user choose this mode
USE_PSEUDO_RELEVANCE_FEEDBACK = True

FOLDER = 'crawled'



def load_files():
    """Loading all the necessary files to run the queries and return ranked urls"""

    global url_from_code, code_from_url, inverted_index, docs_length, page_ranks, docs_tokens

    with open('url_from_code_dict.pickle', 'rb') as handle:
        url_from_code = pickle.load(handle)
    with open('code_from_url_dict.pickle', 'rb') as handle:
        code_from_url = pickle.load(handle)
    with open('inverted_index_dict.pickle', 'rb') as handle:
        inverted_index = pickle.load(handle)
    with open('doc_lengths_dict.pickle', 'rb') as handle:
        docs_length = pickle.load(handle)
    with open('page_ranks_dict.pickle', 'rb') as handle:
        page_ranks = pickle.load(handle)
    with open('docs_tokens_dict.pickle', 'rb') as handle:
        docs_tokens = pickle.load(handle)

def query(search):
    global tokenizer, tf_idf_ranker

    load_files()
    tokenizer = CustomTokenizer(N_PAGES)
    tf_idf_ranker = TfidfRanker(inverted_index, N_PAGES, page_ranks, docs_length, True)

    query = search
    query_tokens = tokenizer.tokenize(query)

    best_ranked = tf_idf_ranker.retrieve_most_relevant(query_tokens, USE_PAGE_RANK_EARLY)[:MAX_RESULTS_TO_CONSIDER]
    # handle_normal_query(query_tokens, best_ranked)
    # print(best_ranked)
    n = 10
    choice = display_query_results(best_ranked[:n], url_from_code, query_tokens)
    return choice


def display_query_results(docs_list, url_from_code, query_tokens):
    # url_from_code = Crawler.get_url_from_code()
    msg = "Preprocessed query: "+str(query_tokens)+"\nThese are the results of your query, you can double click" \
                                                   " or select and press ok on a" \
                                                   " result to open the web page in a new tab of your default browser" \
                                                   " or you can choose to Show more results. Press cancel to go back" \
                                                   " to the main menu."
    title = "Query results"
    # print(url_from_code[111])
    # print(docs_list)
    result = []
    for code in docs_list:
        code = list(code)
        code.append(url_from_code[code[0]])
        with open(FOLDER + '/tables/' + str(code[0])) as f:
                doc_text = f.read()
        code.append(doc_text) 
        result.append(code)
    
    print(result)
    # url_list.append("Show more results")
    # url_list.append("Auto expand query")
    # choice = eg.choicebox(msg, title, url_list)
    return result
    


app = Flask(__name__)

@app.route('/')
def hello():
    return render_template('search.html')
@app.route('/inverted', methods = ['POST'])
def inverted():
    print('aaa')
    data = query(request.form['search'])
    # for i in data:
    #     print(i)
    #     print('aaaa')
    #     exit()
    return render_template('result.html', data = data, len = len(data) )


    

app.run(debug=True)


