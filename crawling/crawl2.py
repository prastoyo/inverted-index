from scrapy.spiders import CSVFeedSpider
import scrapy
#from myproject.items import TestItem

class TestItem(scrapy.Item):
    id = scrapy.Field()
    name = scrapy.Field()
    description = scrapy.Field()

class MySpider(CSVFeedSpider):
    name = 'https://uns.ac.id'
    allowed_domains = ['https://uns.ac.id']
    start_urls = ['https://uns.ac.id']
    delimiter = ';'
    quotechar = "'"
    headers = ['id', 'name', 'description']

    def parse_row(self, response, row):
        self.logger.info('Hi, this is a row!: %r', row)
        item = TestItem()
        item['id'] = row['id']
        item['name'] = row['name']
        item['description'] = row['description']
        return item